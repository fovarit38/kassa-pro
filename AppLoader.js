import React, { useEffect } from 'react';
import AppNavigation from './AppNavigation';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Redux Actions
import { setCategories } from './src/redux/actions/ShopActions';
import { stopLoading } from './src/redux/actions/AppActions';

// Components
import Loader from './src/components/general/Loader';

// Services
import Api from './src/services';


const AppLoader = ({ isLoading, setCategories, stopLoading }) => {
    useEffect(_ => {
        Api.getAll()
            .then(({ data }) => {
                if (typeof data === 'string') {
                    console.error(data);
                } else if (data.error) {
                    console.error(data.message);
                } else {
                    setCategories(data.categories);
                }
            })
            .catch(error => console.error(error))
            .finally(stopLoading)
    }, []);

    return isLoading ? <Loader /> : <AppNavigation />;
};

const mapStateToProps = state => state.app;

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        setCategories,
        stopLoading
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(AppLoader);
