import React from 'react';

// Redux
import {ActivityIndicator, AppRegistry, View, PermissionsAndroid, Platform, StyleSheet, Text} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';

//Redux Components
import {createStore} from 'redux';
import store from './src/redux/index';
import {persistStore} from 'redux-persist';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

// Components
import AppLoader from './AppLoader';
import State from './src/redux/actions/StateActions';

const App = () => {
    const persistor = persistStore(store);
    State({tabBar: false});
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}
                         loading={
                             <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}}>
                                 <Text style={{color: '#4C5FC0', fontSize: 44, fontWeight: 'bold'}}> kassa PRO </Text>
                             </View>
                         }>
                <NavigationContainer>
                    <AppLoader/>
                </NavigationContainer>
            </PersistGate>
        </Provider>
    );
};


export default App;
