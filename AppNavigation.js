import React from 'react';

// Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';


// Screens
import HomeScreen from './src/screens/HomeScreen';
import StorageScreen from './src/screens/StorageScreen';
import AuthScreen from './src/screens/auth/AuthScreen';
import ErrorScreen from './src/screens/ErrorScreen';
import EmployeesScreen from './src/screens/EmployeesScreen';
import EmployeesSingleNalogScreen from './src/screens/EmployeesSingleNalogScreen';
import EmployeesSingleNalogPayScreen from './src/screens/EmployeesSingleNalogPayScreen';
import EmployeesSingleScreen from './src/screens/EmployeesSingleScreen';
import RegistrationScreen from './src/screens/auth/RegistrationScreen';
import StorageProductScreen from './src/screens/StorageProductScreen';
import StorageProductSelectScreen from './src/screens/StorageProductSelectScreen';
import StorageProductEditScreen from './src/screens/StorageProductEditScreen';
import NalogScreen from './src/screens/NalogScreen';
import Nalog2Screen from './src/screens/Nalog2Screen';
import Nalog3Screen from './src/screens/Nalog3Screen';
import Nalog4Screen from './src/screens/Nalog4Screen';
import Nalog5Screen from './src/screens/Nalog5Screen';
import Nalog6Screen from './src/screens/Nalog6Screen';
import Nalog7Screen from './src/screens/Nalog7Screen';
import {View} from 'react-native';
import MyTabBar from './src/routers/custom/MyTabBar';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();
const AppNavigation = _ => {
    return (
        <Tab.Navigator
            tabBar={(props) => (
                <View style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                }}>
                    <MyTabBar {...props} />
                </View>
            )}
        >

            <Tab.Screen name="Auth" component={AuthScreen}/>
            <Tab.Screen name="Error" component={ErrorScreen}/>
            <Tab.Screen name="Register" component={RegistrationScreen}/>

            <Tab.Screen name="Home" component={HomeScreen}/>
            <Tab.Screen name="Storage" component={StorageScreen}/>
            <Tab.Screen name="Employees" component={EmployeesScreen}/>
            <Tab.Screen name="EmployeesSingle" component={EmployeesSingleScreen}/>
            <Tab.Screen name="EmployeesSingleNalog" component={EmployeesSingleNalogScreen}/>
            <Tab.Screen name="EmployeesSingleNalogPay" component={EmployeesSingleNalogPayScreen}/>
            <Tab.Screen name="StorageProduct" component={StorageProductScreen}/>
            <Tab.Screen name="StorageProductSelect" component={StorageProductSelectScreen}/>
            <Tab.Screen name="StorageProductEdit" component={StorageProductEditScreen}/>
            <Tab.Screen name="Nalog" component={NalogScreen}/>
            <Tab.Screen name="Nalog2" component={Nalog2Screen}/>
            <Tab.Screen name="Nalog6" component={Nalog6Screen}/>
            <Tab.Screen name="Nalog5" component={Nalog5Screen}/>
            <Tab.Screen name="Nalog4" component={Nalog4Screen}/>
            <Tab.Screen name="Nalog3" component={Nalog3Screen}/>
            <Tab.Screen name="Nalog7" component={Nalog7Screen}/>

        </Tab.Navigator>
    );
};


export default AppNavigation;
