import React from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Redux Actions
import { removeError } from '../redux/actions/AppActions';

// Components
import Navbar from '../components/general/Navbar';
import Header from '../components/general/Header';
import StocksSlider from '../components/sliders/StocksSlider';
import CategoriesList from '../components/categories/CategoriesList';
import ErrorModal from '../components/general/ErrorModal';

const HomeScreen = ({ navigation, removeError }) => {
    const showProductsHandler = categoryId => {
        removeError();
        navigation.navigate('Menu', {
            categoryId
        });
    };

    return (
        <>
            <View style={styles.container}>
                <Header />
                <ScrollView style={styles.scrollView}>
                    <StocksSlider />
                    <CategoriesList showProductsHandler={showProductsHandler} />
                </ScrollView>
            </View>
            <Navbar activeButtonIndex={0} navigation={navigation} />
            <ErrorModal />
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        paddingHorizontal: 15,
    },
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        removeError
    }, dispatch)
)

export default connect(null, mapDispatchToProps)(HomeScreen);
