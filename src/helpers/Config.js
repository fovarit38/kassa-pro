import config from '../../config';

class Config {
    static get(key) {
        return config[key] || null;
    }
}

export default Config;
