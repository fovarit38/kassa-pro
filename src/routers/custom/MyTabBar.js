import React, {useState} from 'react';
import {
    ActivityIndicator, Alert, Button,
    Image, Platform, SafeAreaView, ScrollView,
    StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import {useNavigation, useNavigationState} from '@react-navigation/native';
import Css from '../../utils/css';
import Rem from '../../utils/rem';
import Modal from 'react-native-modal';
import store from '../../redux/index';
import Navbar from '../../components/general/Navbar';
import {connect} from 'react-redux';
import ModifiedText from '../../components/general/ModifiedText';
import State from '../../redux/actions/StateActions';


function MyTabBar({navigation, descriptors, state}) {

    function setModalVisible(event) {
        State({message_show: false});
    }

    let activeButtonIndex = 0;

    if (store.getState().state.is_auth && !store.getState().state.routes) {
        State({routes: true});
        navigation.navigate('Basket');
    }
    const buttons = [
        {
            title: 'Главная',
            defaultIcon: require('../../../assets/images/propIcon/fi_home.png'),
            activeIcon: require('../../../assets/images/propIcon/fi_home.png'),
            targetScreenName: 'Home',
        },
        {
            title: 'Касса',
            defaultIcon: require('../../../assets/images/propIcon/u_receipt.png'),
            activeIcon: require('../../../assets/images/propIcon/u_receipt.png'),
            targetScreenName: 'Home',
        },
        {
            title: 'Документы',
            defaultIcon: require('../../../assets/images/propIcon/fi_file.png'),
            activeIcon: require('../../../assets/images/propIcon/fi_file.png'),
            targetScreenName: 'Home',
        },
        {
            title: 'Кабинет',
            defaultIcon: require('../../../assets/images/propIcon/fi_profile.png'),
            activeIcon: require('../../../assets/images/propIcon/fi_profile.png'),
            targetScreenName: 'Профиль',
        },
    ];
    const content = buttons.map((item, index) => (


        <TouchableOpacity
            key={index}
            style={[styles.button]}
            onPress={_ => {
                if (index != 0) {
                    Alert.alert('Доступ закрыт');
                }
                // State({select_bar: index});
                navigation.navigate(item.targetScreenName);
            }
            }
        >
            <Image
                style={[styles.icon, {opacity: item.title == 'Бонусы' ? 0.25 : 1}]}
                // source={item.defaultIcon}
                source={store.getState().state.select_bar === index ? item.activeIcon : item.defaultIcon}
            />
            <ModifiedText
                text={item.title}
                width={500}
                style={{
                    fontSize: 12,
                    color: '#1A1C1E',
                    opacity: item.title == 'Бонусы' ? 0.25 : 1,
                }}
            />
        </TouchableOpacity>
    ));


    return (
        <View
            style={{
                position: 'relative',
                flex: 1,
                opacity: store.getState().state.tabBar ? 1 : 0,
                justifyContent: 'flex-end',
            }}
        >
            <View style={{
                flexDirection: 'row',
                position: 'relative',
                backgroundColor: '#fff',
                justifyContent: 'space-between',
                alignItems: 'center',

            }}>
                <View style={styles.container}>{content}</View>
            </View>

            <Modal
                isVisible={store.getState().state.message_show}
                onSwipeComplete={() => setModalVisible(false)}
                onBackdropPress={() => setModalVisible(false)}
            >
                <View style={styles.container}>
                    <View style={styles.container_def}>
                        <View style={styles.model_natif}>
                            <ModifiedText style={styles.header} width={600} text={store.getState().state.message_}/>

                            <View style={styles.model_natif_box}>
                                <Button color="#C82F5C" title="Продолжить " onPress={_ => {
                                    setModalVisible(false);
                                }}/>
                                <Button
                                    icon={{
                                        name: 'arrow-right',
                                        size: 15,
                                        color: 'red',
                                    }}
                                    style={styles.btn2} title="в корзину"
                                    onPress={_ => {
                                        setModalVisible(false);
                                        navigation.navigate('Basket');
                                    }}/>

                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>

    );
}


let css_ = {
    container: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        paddingTop: 10,
        paddingBottom: Platform.OS === 'ios' ? 25 : 10,
        borderTopWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.07)',
    },
    button: {
        alignItems: 'center',
    },
    icon: {
        width: 30,
        height: 28,
        marginBottom: 4,
        resizeMode: 'contain',
    },
    model_natif: {
        alignItems: 'center',
        paddingHorizontal: 25,
        paddingTop: 30,
        paddingBottom: 14,
        backgroundColor: '#ffffff',
        borderRadius: 16,
    },
    header: {
        marginBottom: 25,
        fontSize: 18,
        color: '#C82F5C',
    },
    message: {
        marginBottom: 30,
        fontSize: 16,
        textAlign: 'center',
    },
    model_natif_box: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    btn2: {
        backgroundColor: 'red',
        borderWidth: 10,
    },
};

const styles = StyleSheet.create({
    ...Css(),
    ...css_,
});


const mapStateToProps = state => ({
    tabBar: state.state.tabBar,
    state_data: state.state.state,
    is_auth: state.state.is_auth,
    message_: state.state.message_,
    message_show: state.state.message_show,
});

export default connect(mapStateToProps)(MyTabBar);
