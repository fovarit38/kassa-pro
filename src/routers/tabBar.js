import React from 'react';
import {
    Image, SafeAreaView, ScrollView,
    StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {BottomTabBar, createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import MyTabBar from '../../src/routers/custom/MyTabBar';


const Tab = createBottomTabNavigator();

class TabBar extends React.Component {
    render() {

        return (
            <Tab.Navigator
                tabBar={(props) => (
                    <View style={{
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        width: '100%',
                    }}>
                        <MyTabBar {...props} />
                    </View>
                )}
            >
                {/*<Tab.Screen name="main"  component={main}/>*/}
                {/*<Tab.Screen name="compos"  component={compos}/>*/}
                {/*<Tab.Screen name="Menu"  component={Menu}/>*/}


            </Tab.Navigator>
        );

    }
}


export default TabBar;
