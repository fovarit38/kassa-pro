import {applyMiddleware, combineReducers, createStore, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';
import {persistStore, persistReducer, persistCombineReducers} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createWhitelistFilter} from 'redux-persist-transform-filter';
import thunk from 'redux-thunk';

// Reducers
import appReducer from './reducers/AppReducer';
import shopReducer from './reducers/ShopReducer';
import stateReducer from './reducers/StateReducer';

const saveSubsetWhitelistFilter = createWhitelistFilter('', ['auth']);


const persistConfig = {
    key: 'primary',
    storage: AsyncStorage,
    transform: [saveSubsetWhitelistFilter],
    blacklist: [],
};

const rootReducer = persistCombineReducers(persistConfig, {
    app: appReducer,
    shop: shopReducer,
    state: stateReducer
});



let store = createStore(rootReducer);
persistStore(store, () => {
});

export default store;
