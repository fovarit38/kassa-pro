export const startLoading = _ => ({ type: 'START_LOADING' });
export const stopLoading = _ => ({ type: 'STOP_LOADING' });

export const setError = errorMessage => ({
    type: 'SET_ERROR',
    payload: errorMessage
});

export const removeError = _ => ({ type: 'REMOVE_ERROR' });
