import shopReducer from './ShopReducer';

const INITIAL_STATE = {
    tabBar: false,
    menu_send: false,
    selected_catalog: [],
    select_bar: 1,
    selected_catalog_products: [],
    selected_product: [],
    basket: [],
    basket_list_id: [],
    profile: [],
    stocks: [],
    stocksOne: [],
    favorites: [],
    employees: [],
    employees_select: {},
    employees_select_index: 0,
    products: [],
    productSelect: {},
    productSelect_index: 0,
    search_info: false,
    user_token: '',
    is_auth: false,
    routes: false,
    message_: '',
    message_show: false,
    strih_code: '',
};


// {
//     name: 'Аскаров Алмас Алтайулы',
//         iin: '010392751843',
//     price: '250000',
// },


const stateReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'DATA':
            return {...state, ...action.value};
        default:
            return state;
    }
};
export default stateReducer;
