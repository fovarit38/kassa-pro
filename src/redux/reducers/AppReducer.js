const INITIAL_STATE = {
    isLoading: true,
    error: null
};

const appReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'START_LOADING':
            return {
                ...state,
                isLoading: true
            };
        case 'STOP_LOADING':
            return {
                ...state,
                isLoading: false
            };
        case 'SET_ERROR':
            return {
                ...state,
                error: action.payload
            };
        default:
            return state;
    }
};

export default appReducer;
