import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

// Components
import ModifiedText from './ModifiedText';

const Loader = _ => {
    return (
        <View style={styles.container}>

            <ModifiedText style={{ fontSize: 16 }} width={300} text='Идёт загрузка ...' />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: '80%',
        height: 100,
        resizeMode: 'contain',
        marginBottom: 30,
    },
});

export default Loader;
