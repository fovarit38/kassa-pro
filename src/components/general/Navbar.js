import React from 'react';
import { View, StyleSheet, Image, Platform, TouchableOpacity } from 'react-native';

// Components
import ModifiedText from './ModifiedText';

const Navbar = ({ activeButtonIndex = 0, navigation }) => {
    const buttons = [
        {
            title: 'Главная',
            defaultIcon: null,
            activeIcon: null,
            targetScreenName: 'Home'
        },
        {
            title: 'Меню',
            defaultIcon: null,
            activeIcon: null,
            targetScreenName: 'Menu'
        },
        {
            title: 'Корзина',
            defaultIcon: null,
            activeIcon: null,
            targetScreenName: 'Home'
        },
        {
            title: 'Бонусы',
            defaultIcon: null,
            activeIcon: null,
            targetScreenName: 'Home'
        },
        {
            title: 'Ещё',
            defaultIcon: null,
            activeIcon: null,
            targetScreenName: 'Home'
        }
    ];

    const content = buttons.map((item, index) => (
        <TouchableOpacity
            key={index}
            style={styles.button}
            onPress={_ => navigation.navigate(item.targetScreenName)}
        >
            {/*<Image*/}
            {/*    style={styles.icon}*/}
            {/*    source={activeButtonIndex === index ? item.activeIcon : item.defaultIcon}*/}
            {/*/>*/}
            <ModifiedText
                text={item.title}
                width={500}
                style={{ color: activeButtonIndex === index ? '#DE374B' : '#B3C0D3' }}
            />
        </TouchableOpacity>
    ));

    return <View style={styles.container}>{content}</View>;
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        paddingTop: 10,
        paddingBottom: Platform.OS === 'ios' ? 25 : 10,
        borderTopWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.07)'
    },
    button: {
        alignItems: 'center',
    },
    icon: {
        width: 30,
        height: 30,
        marginBottom: 4,
        resizeMode: 'contain',
    },
});

export default Navbar;
