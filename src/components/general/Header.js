import React, {useState} from 'react';
import {View, Image, StyleSheet, TouchableOpacity, TextInput, Text} from 'react-native';
import Css from '../../utils/css/index';
import {useNavigation} from '@react-navigation/native';
import Back from '../btn/Back';

const Header = ({seatch, name, click, next = true, icon=3, scanner}) => {

    const navigation = useNavigation();
    const [search, setSearch] = useState({text: ''});
    return (
        <View style={[styles.fullBox, styles.headers]}>


            <Back
                icon={2}
                click={() => {
                    if (typeof click != 'undefined') {
                        click();
                    } else {
                        navigation.goBack();
                    }
                }}
            />
            <Text
                style={[styles.logoText, styles.text, styles.textBold, styles.text20, {marginLeft: -4}]}> {name} </Text>

            <Back
                icon={icon}
                next={next}
                click={scanner}
            />

        </View>
    );
};


let cssThus = {};
const styles = StyleSheet.create(
    {
        ...cssThus,
        ...Css(),
    },
);

export default Header;
