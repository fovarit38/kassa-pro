import React, { useState } from 'react';
import { View, Button, StyleSheet } from 'react-native';

// Components
import ModifiedText from './ModifiedText';
import Modal from 'react-native-modal';

const ErrorModal = ({ isVisible, error }) => {
    const [flag, setFlag] = useState(true);

    return (
        <Modal isVisible={isVisible && flag}>
            <View style={styles.container}>
                <ModifiedText style={styles.header} width={600} text='Усп... Что-то произошло :(' />
                <ModifiedText style={styles.message} text={error} />

                <Button title='Закрыть' onPress={_ => setFlag(false)} />
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingHorizontal: 25,
        paddingTop: 30,
        paddingBottom: 14,
        backgroundColor: '#ffffff',
        borderRadius: 16,
    },
    header: {
        marginBottom: 25,
        fontSize: 18,
        color: '#C82F5C',
    },
    message: {
        marginBottom: 30,
        fontSize: 16,
        textAlign: 'center'
    },
});

export default ErrorModal;
