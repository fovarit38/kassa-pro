import React from 'react';
import { Text } from 'react-native';

const ModifiedText = ({ width = 400, isItalic = false, text = '', style = {} }) => {
    let fontFamily = 'SFProText-';

    switch (width) {
        case 100:
            fontFamily += 'Thin';
            break;
        case 200:
            fontFamily += 'Ultralight';
            break;
        case 300:
            fontFamily += 'Light';
            break;
        case 400:
            fontFamily += 'Regular';
            break;
        case 500:
            fontFamily += 'Medium';
            break;
        case 600:
            fontFamily += 'Semibold';
            break;
        case 700:
            fontFamily += 'Bold';
            break;
        case 800:
            fontFamily += 'Heavy';
            break;
        case 900:
            fontFamily += 'Black';
            break;
        default:
            fontFamily += 'Regular';
    }

    if (isItalic) {
        fontFamily += 'Italic';
    }

    return <Text style={{ ...style, fontFamily }}>{text}</Text>;
};

export default ModifiedText;
