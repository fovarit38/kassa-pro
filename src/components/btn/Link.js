import React from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions

// Components
import Css from '../../utils/css';
import Rem from '../../utils/rem';
import State from '../../redux/actions/StateActions';
import {useNavigation} from '@react-navigation/native';

const Link = ({name, click}) => {
    const navigation = useNavigation();

    return (
        <>
            <TouchableOpacity
                onPress={() => {
                    if (typeof click != 'undefined') {
                        click();
                    }
                }}
                style={styles.link}>
                <Text style={[styles.text, styles.text18, styles.textBold, styles.textColor3]}>{name}</Text>
            </TouchableOpacity>
        </>
    );
};

const styles = StyleSheet.create(
    {
        ...Css(),
        ...{
            container: {
                flex: 1,
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
    },
);

export default Link;
