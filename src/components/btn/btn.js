import React from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions

// Components
import Css from '../../utils/css';
import Rem from '../../utils/rem';
import {useNavigation} from '@react-navigation/native';

const Btn1 = ({name, icon, width, height, click}) => {

    const navigation = useNavigation();

    return (
        <>
            <TouchableOpacity
                onPress={click}
                style={styles.btnAuth}>
                <Text style={[styles.text, styles.textColor2, styles.textBold, styles.text16]}>{name}</Text>
            </TouchableOpacity>
        </>
    );
};


const styles = StyleSheet.create(
    {
        ...Css(),
        ...{
            container: {
                flex: 1,
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
    },
);

export default Btn1;
