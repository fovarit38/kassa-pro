import React from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions

// Components
import Css from '../../utils/css';
import Rem from '../../utils/rem';
import {useNavigation} from '@react-navigation/native';

const Back = ({name, icon, width, height, click, next = true}) => {
    const navigation = useNavigation();

    icon = icon != null ? icon : 1;

    if (icon == 1) {
        icon = require('../../../assets/images/backHeader.png');
    }

    if (icon == 2) {
        icon = require('../../../assets/images/backHeader.png');
    }
    if (icon == 3) {
        icon = require('../../../assets/images/u_barcode-scan.png');
    }
    if (icon == 4) {
        icon = require('../../../assets/images/fi_edit.png');
    }
    return (
        <>
            <TouchableOpacity
                onPress={click}>
                {
                    next && (
                        <Image
                            style={styles.backImg}
                            resizeMode="contain"
                            source={(icon)}
                        />
                    )
                }
            </TouchableOpacity>
        </>
    );
};

const styles = StyleSheet.create(
    {
        ...Css(),
        ...{
            container: {
                flex: 1,
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
    },
);

export default Back;
