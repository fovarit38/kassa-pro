import * as React from 'react';
import {View, SafeAreaView, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';

// Redux
import {connect} from 'react-redux';

// Components
import Carousel from 'react-native-snap-carousel';
import ModifiedText from '../general/ModifiedText';

import State from '../../redux/actions/StateActions';

// Constants
import {ASSETS_PATH} from '../../../config';
import Api from '../../services';
import {useState} from 'react';

/**
 * @param categories
 * @param exceptCategoryId Specify category ID to prevent render of chosen category.
 * @returns {JSX.Element}
 * @constructor
 */
const CategoriesSlider = ({categories, exceptCategoryId = null}) => {



    const renderItem = ({item}) => {
        return (
            // selected_catalog
            <TouchableOpacity
                onPress={() => {
                    State({selected_catalog: item,selected_catalog_products:[]});
                    Api.getProducts(item["id"]);
                }}
                style={styles.slide}>
                <ImageBackground
                    style={styles.slideImageBackground}
                    source={{uri: ASSETS_PATH + '/' + item.image}}
                >
                    <View style={styles.slideTitleWrapper}>
                        <ModifiedText style={{fontSize: 14, textAlign: 'center'}} text={item.name}/>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        );
    };

    return (
        <SafeAreaView
            style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: 15, marginHorizontal: 10}}>
            <Carousel
                // data={categories.filter(item => item.id !== exceptCategoryId)}
                data={categories}
                sliderWidth={120}
                itemWidth={120}
                renderItem={renderItem}
                inactiveSlideOpacity={1}
                inactiveSlideScale={1}
                slideStyle={{marginHorizontal: 5}}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    slide: {
        alignItems: 'center',
        backgroundColor: '#dedce5',
        borderRadius: 16,
        height: 120,
    },
    slideImageBackground: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
        borderRadius: 16,
        overflow: 'hidden',
    },
    slideTitleWrapper: {
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: 15,
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        borderRadius: 58,
    },
});

const mapStateToProps = state => ({
    categories: state.shop.categories,
    selected_catalog: state.state.selected_catalog,
});

export default connect(mapStateToProps)(CategoriesSlider);
