import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';
import Carousel,{Pagination } from 'react-native-snap-carousel';
import State from '../../redux/actions/StateActions';
import Rem from '../../utils/rem';
import Api from '../../services';
import {API_PATH, ASSETS_PATH} from '../../../config';
import ModifiedText from '../general/ModifiedText';

const StocksSlider = _ => {

    const [stocks, setStocks] = useState([]);
    const [load, setLoad] = useState(false);
    const [index_slide, setIndex] = useState(false);


    useEffect(_ => {
        if (!load) {
            fetch(`${API_PATH}/stocks`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => res.json())
                .then(res => {
                    setLoad(true);
                    setStocks(res["stocks"]);
                });
        }
    });


    const renderItem = ({item}) => {
        return (
            <ImageBackground
                style={styles.slide}
                source={{uri: ASSETS_PATH + '/' + item.image}}
            />
        );
    };

    return (
        <View style={styles.container}>
            <Carousel
                // data={categories.filter(item => item.id !== exceptCategoryId)}
                data={stocks}
                sliderWidth={Rem(344)}
                itemWidth={Rem(344)}
                renderItem={renderItem}
                inactiveSlideOpacity={1}
                inactiveSlideScale={1}
                onSnapToItem={(index) => setIndex(index) }
            />
            <Pagination
                dotsLength={stocks.length}
                activeDotIndex={index_slide}
                containerStyle={{
                    position: 'absolute',
                    bottom:0,
                    right:10,
                }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    marginHorizontal: 0,
                    backgroundColor: '#F7F6FE',
                    bottom:-5
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={1}
            />
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 250,
        marginVertical: 15,
    },
    slide: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 16,
        overflow: 'hidden',
    },
});

export default StocksSlider;
