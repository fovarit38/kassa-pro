import React from 'react';
import {View, ImageBackground, StyleSheet, Dimensions} from 'react-native';

// Redux
import {connect} from 'react-redux';

// Components
import ModifiedText from '../general/ModifiedText';
import {TouchableOpacity} from 'react-native';

// Constants
import {ASSETS_PATH} from '../../../config';

const cardSize = Dimensions.get('window').width * 0.49 - 18;
const cardMargin = Dimensions.get('window').width * 0.02 + 5;

import {useNavigation, useNavigationState} from '@react-navigation/native';
import State from '../../redux/actions/StateActions';
import Api from '../../services';

const CategoriesList = ({categories}) => {
    let marginMode = 0;

    const navigation = useNavigation();

    const slides = categories.map((item, index) => {

        let cardStyles = {...styles.item, marginBottom: cardMargin};
        if (item.size === 'big' && (index % 2 === 0 || categories[index - 1]?.size === 'big' || index === 0)) {
            marginMode = marginMode === 1 ? 0 : 1;
            cardStyles = {
                ...cardStyles,
                width: '100%',
            };
        } else {
            cardStyles = {
                ...cardStyles,
                marginRight: index % 2 === marginMode ? cardMargin : 0,
            };
        }

        return (
            <View key={item.id} style={cardStyles}>
                <TouchableOpacity activeOpacity={0.8} onPress={() => {
                    navigation.navigate('Menu');
                    State({selected_catalog: item, selected_catalog_products: []});
                    Api.getProducts(item['id']);
                }}>
                    <ImageBackground
                        style={styles.image}
                        source={{uri: ASSETS_PATH + '/' + item.image}}
                    >
                        <View style={styles.titleWrapper}>
                            <ModifiedText style={{fontSize: 16, textAlign: 'center'}} width={600} text={item.name}/>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
            </View>
        );
    });

    return (
        <View style={styles.container}>{slides}</View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    item: {
        width: cardSize,
        height: cardSize,
        borderRadius: 16,
        overflow: 'hidden',
    },
    image: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
    },
    titleWrapper: {
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: 15,
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        borderRadius: 58,
    },
});

export default connect(state => state.shop)(CategoriesList);
