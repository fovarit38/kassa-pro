import React from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions

// Components
import Css from '../../utils/css';
import Rem from '../../utils/rem';
import {useNavigation} from '@react-navigation/native';

const Radio = ({name, icon, width, height, click, active}) => {

    const navigation = useNavigation();

    return (
        <>
            <TouchableOpacity
                onPress={click}
                style={[styles.phoneInput, styles.text, styles.mb20, styles.text16, {
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                }]}
            >
                <Text style={[styles.text, styles.text16]}>{name}</Text>
                {active && (<Image
                    style={styles.activeRadio}
                    resizeMode="cover"
                    source={require('../../../assets/images/check-box.png')}
                />)}
            </TouchableOpacity>
        </>
    );
};


const styles = StyleSheet.create(
    {
        ...Css(),
        ...{
            container: {
                flex: 1,
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
    },
);

export default Radio;
