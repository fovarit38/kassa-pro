import React, {useState} from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TextInput} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';


// Components
import Css from '../../utils/css';
import Rem from '../../utils/rem';

const Input = ({name, place, editable, onChangeText, value, secret, returnSetVal}) => {

    const [vas, setVas] = useState(value);
    if (returnSetVal) {
        returnSetVal(setVas, name);
    }
    return (
        <>
            <View style={[styles.inputTel]}>
                <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.text12, {
                        color: '#919191',
                        marginBottom: 5,
                        width: 10,
                        flexGrow: 2,
                    }]}>{name}</Text>
                </View>
                <View>
                    <TextInput
                        onChangeText={(e) => {
                            if (typeof onChangeText != 'undefined') {
                                onChangeText(e);
                            }
                            setVas(e);
                        }}
                        value={vas}
                        placeholder={place}
                        secureTextEntry={secret}
                        style={[styles.phoneInput, styles.text, styles.text16]}
                    />
                    {secret && (<Image
                        style={styles.icon_secret}
                        resizeMode="cover"
                        source={require('../../../assets/images/fi_eye.png')}
                    />)}
                </View>
            </View>
        </>
    );
};


const styles = StyleSheet.create(
    {
        ...Css(),
        ...{
            container: {
                flex: 1,
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
    },
);

export default Input;
