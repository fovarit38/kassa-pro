import React from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TextInput} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';


// Components
import Css from '../../utils/css';
import Rem from '../../utils/rem';

const inputSearch = ({name, place, editable, onChangeText, value, secret}) => {

    return (
        <>
            <View style={[styles.inputTel]}>
                <View>

                    <TextInput
                        onChangeText={onChangeText}
                        // value={phone['text']}
                        placeholder={place}
                        secureTextEntry={secret}
                        style={[styles.phoneInput, styles.text, styles.text16]}
                    />

                </View>
            </View>
        </>
    );
};


const styles = StyleSheet.create(
    {
        ...Css(),
        ...{
            container: {
                flex: 1,
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
    },
);

export default inputSearch;
