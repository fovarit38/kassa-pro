import React, {useState} from 'react';
import {View, ImageBackground, StyleSheet, Dimensions, Text, Image} from 'react-native';

// Redux
import {connect} from 'react-redux';

// Components
import ModifiedText from '../general/ModifiedText';
import {TouchableOpacity} from 'react-native';

// Constants
import {ASSETS_PATH} from '../../../config';

const cardSize = Dimensions.get('window').width * 0.49 - 18;
const cardMargin = Dimensions.get('window').width * 0.02 + 5;

import {useNavigation, useNavigationState} from '@react-navigation/native';
import State from '../../redux/actions/StateActions';
import Api from '../../services';
import Header from '../general/Header';
import Modal from 'react-native-modal';
import Btn from '../btn/btn';
import Rem from '../../utils/rem';
import Css from '../../utils/css';
import Radio from '../radio/Radio';
import Input from '../input/input';
import store from '../../redux';

const Components = ({categories, openModel, add = true, employees_select_index}) => {
    let marginMode = 0;

    const navigation = useNavigation();
    const [ModalVisible, setModalVisibleMessage] = useState(false);
    const [index, setId] = useState(0);

    const [formDataVal, setFormData] = useState({});


    let formData = [
        {
            name: 'Регистрация владельца ИП',
            input: [
                // {name: 'who', place: 'Выбрать файл', val: 'ЭЦП', type: 'input', secret: false},
                {name: 'name', place: 'Арманов Ильяс Сапарулы', val: 'ФИО', type: 'input', secret: false},
                {name: 'iin', place: 'ХХХХ-ХХХХ-ХХХХ', val: 'ИИН', type: 'input', secret: false},
                {name: 'price', place: '250 000 ₸', val: 'Заработная плата', type: 'input', secret: false},
                {name: 'pass', place: '', val: 'Пароль сотрудника', type: 'input', secret: false},
            ],
        },
    ];
    // let preFormDataVal = {};

    if (openModel) {
        openModel(setModalVisibleMessage, setFormData, setId);
    }

    return (
        <View>

            <Modal
                onSwipeComplete={() => setModalVisibleMessage(false)}
                onBackdropPress={() => setModalVisibleMessage(false)}
                isVisible={ModalVisible}
                style={{margin: 0, justifyContent: 'center', alignItems: 'center'}}
                swipeDirection="right"
                propagateSwipe
                animationIn="slideInRight"
                animationOut="slideOutRight"

            >
                <View style={{flex: 1, backgroundColor: '#fff'}}>
                    <Header click={() => {
                        setModalVisibleMessage(false);
                    }} next={false} name={'Добавить сотрудника'}/>

                    <View style={{alignItems: 'center'}}>
                        {
                            formData[0].input.map((input, index) => {
                                    if (input.type == 'radio') {
                                        return (
                                            <Radio click={() => {
                                                preFormDataVal[input.name] = input.val;
                                            }}
                                                   active={typeof formDataVal[input.name] != 'undefined' && formDataVal[input.name] == input.val}
                                                   name={input.val}/>
                                        );
                                    }
                                    if (input.type == 'input') {
                                        return (
                                            <Input name={input.val} place={input.place} value={formDataVal[input.name]}
                                                   secret={input.secret}
                                                   onChangeText={(e) => {
                                                       let saveInfo = formDataVal;
                                                       saveInfo[input.name] = e;
                                                       setFormData(saveInfo);
                                                   }}/>
                                        );
                                    }

                                    return null;

                                },
                            )
                        }
                        <Btn click={() => {
                            // console.log(preFormDataVal);
                            // console.log();
                            let insfo = store.getState().state.employees;
                            if (add) {
                                insfo.push({
                                    name: formDataVal?.name,
                                    iin: formDataVal?.iin,
                                    price: formDataVal?.price,
                                });
                                State({employees: insfo});

                            } else {
                                insfo[employees_select_index] = {
                                    name: formDataVal.name ? formDataVal.name : insfo[employees_select_index].name,
                                    iin: formDataVal.iin ? formDataVal.iin : insfo[employees_select_index].iin,
                                    price: formDataVal.price ? formDataVal.price : insfo[employees_select_index].price,
                                };
                                State({employees: insfo, employees_select: insfo[employees_select_index]});
                            }
                            // setModalVisibleMessage(false);
                            setModalVisibleMessage(false);

                        }} name={(
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text style={{color: '#fff'}}>{add ? 'Добавить' : 'Изменить'} </Text>
                            </View>
                        )}/>
                    </View>
                </View>
            </Modal>

            {
                add && (
                    <Btn click={() => {
                        setModalVisibleMessage(true);
                    }} name={(
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Image
                                style={{width: Rem(24), height: Rem(24), marginRight: Rem(7)}}
                                resizeMode="contain"
                                source={require('../../../assets/images/fi_user-plus.png')}
                            />
                            <Text style={{color: '#fff'}}>Добавить сотрудника</Text>
                        </View>
                    )}/>
                )
            }
        </View>
    );
};


const mapStateToProps = state => ({
    employees: state.state.employees,
    employees_select_index: state.state.employees_select_index,
});

export default connect(mapStateToProps, null)(Components);

