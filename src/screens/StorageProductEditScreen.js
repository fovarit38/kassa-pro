import React, {useEffect} from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity, TextInput} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import BarcodeScanner from 'react-native-scan-barcode';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';
import InputSearch from '../components/input/inputSearch';
import Radio from '../components/radio/Radio';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import Rem from '../utils/rem';
import store from '../redux';
import State from '../redux/actions/StateActions';

class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            bearCode: false,
            torchMode: 'off',
            cameraType: 'back',
            codeLocation: '',
            formDataVal: this.props.productSelect,
            formData: {
                input: [
                    // {name: 'who', place: 'Выбрать файл', val: 'ЭЦП', type: 'input', secret: false},
                    {
                        name: 'name',
                        place: 'Введите название',
                        val: 'Название товара',
                        type: 'input',
                        secret: false,
                        valdef: '',
                    },
                    {name: 'price', place: '0 ₸', val: 'Цена', type: 'input', secret: false, valdef: ''},
                    {
                        name: 'ed',
                        place: 'Единица измерения ',
                        val: 'Единица измерения',
                        type: 'input',
                        secret: false,
                        valdef: '',
                    },
                    {name: 'count', place: '0 шт', val: 'Количество', type: 'input', secret: false, valdef: ''},
                    {name: 'brand', place: 'BOSCH', val: 'Бренд', type: 'input', secret: false, valdef: ''},
                    {name: 'color', place: 'Черный', val: 'Цвет', type: 'input', secret: false, valdef: ''},
                ],
            },
        };


    }

    barcodeReceived(e) {
        State({
            strih_code: e.data,
            search_info: true,
        });
        this.setState({bearCode: false});
    }


    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    setModalVisibleScanner(event) {
        this.setState({bearCode: event});

    }

    render() {

        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Modal
                            onSwipeComplete={() => this.setModalVisibleScanner(false)}
                            onBackdropPress={() => this.setModalVisibleScanner(false)}
                            isVisible={this.state.bearCode}
                            style={{margin: 0, justifyContent: 'center', alignItems: 'center'}}
                            swipeDirection="right"
                            propagateSwipe
                            animationIn="slideInRight"
                            animationOut="slideOutRight"

                        >
                            <View style={{flex: 1, backgroundColor: '#fff', width: '100%', position: 'relative'}}>
                                <BarcodeScanner
                                    onBarCodeRead={(e) => {
                                        this.barcodeReceived(e);
                                    }}
                                    style={{flex: 1}}
                                    torchMode={this.state.torchMode}
                                    cameraType={this.state.cameraType}

                                />
                                <TouchableOpacity style={{
                                    position: 'absolute',
                                    zIndex: 10,
                                    width: Rem(207),
                                    height: Rem(36),
                                    bottom: Rem(27),
                                    left: Rem(16),
                                    backgroundColor: '#FFFFFF',
                                    borderRadius: Rem(10),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Text style={[styles.text, styles.text13, {color: '#009FF5'}]}>Ввести штрих код
                                        вручную</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{
                                    position: 'absolute',
                                    zIndex: 10,
                                    top: Rem(27),
                                    right: Rem(16),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                                                  onPress={() => {
                                                      this.setState({
                                                          bearCode: false,
                                                      });
                                                  }}
                                >
                                    <Image
                                        style={styles.backImg}
                                        resizeMode="contain"
                                        source={require('../../assets/images/fi_x.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                        </Modal>


                        <Header scanner={() => {
                            this.setState({
                                bearCode: true,
                            });
                        }} next={false} name={'Добавить товар'}/>

                        <View style={styles.containerBottoms}>
                            <View style={[styles.fullBox, styles.mt20]}>

                                {
                                    this.state.formData.input.map((input, index) => {

                                            if (input.type == 'input') {
                                                return (
                                                    <View style={{position: 'relative'}}>
                                                        <Input name={input.val} place={input.place} secret={input.secret}
                                                               value={input.name == 'code' && this.state.codeLocation}
                                                               onChangeText={(e) => {
                                                                   let preFormDataVal = this.state.formDataVal;
                                                                   preFormDataVal[input.name] = e;
                                                                   this.setState({formDataVal: preFormDataVal});
                                                               }}/>
                                                    </View>
                                                );
                                            }

                                            return null;

                                        },
                                    )
                                }

                                <Btn click={() => {

                                    let insfo = store.getState().state.products;

                                    insfo[this.props.productSelect_index] = ({
                                        code: this.state.formDataVal?.code,
                                        count: this.state.formDataVal?.count,
                                        price: this.state.formDataVal?.price,
                                        ed: this.state.formDataVal?.ed,
                                        name: this.state.formDataVal?.name,
                                        brand: this.state.formDataVal?.brand,
                                        color: this.state.formDataVal?.color,
                                    });
                                    // setModalVisibleMessage(false);
                                    this.props.navigation.navigate('Storage');
                                    State({products: insfo});

                                }} name={(
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Text style={{color: '#fff'}}>Изменить</Text>
                                    </View>
                                )}/>

                            </View>


                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    productSelect: state.state.productSelect,
    productSelect_index: state.state.productSelect_index,
    search_info: state.state.search_info,
});

export default connect(mapStateToProps, null)(StorageScreen);
