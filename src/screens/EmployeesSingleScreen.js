import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator, Alert,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';
import InputSearch from '../components/input/inputSearch';
import Btn from '../components/btn/btn';
import {style} from 'redux-logger/src/diff';
import {request} from 'react-native-permissions';

class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            openModel: false,
            setOpenModel: null,
            setFormData: null,
            setId: null,
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }


    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Header scanner={() => {
                            this.state.setFormData(this.props.employees_select);
                            this.state.setId(this.props.employees_select_index);
                            this.state.setOpenModel(true);
                        }} icon={4} name={'Сотрудники'}/>

                        <View style={[styles.container_def, styles.containerBottoms, {marginTop: Rem(0)}]}>

                            <AddEmployees add={false} init={(target, formData, setId) => {
                                if (this.state.setOpenModel == null) {
                                    this.setState({setOpenModel: target, setFormData: formData, setId: setId});
                                }
                            }}/>

                            <Text style={[styles.text, styles.text17, styles.textBold, styles.textColor1A1C1E]}>
                                {this.props.employees_select?.name}
                            </Text>

                            <View style={[styles.infoEmployess, {marginTop: Rem(30)}]}>
                                <View style={styles.infoEmployess_name}>
                                    <Text
                                        style={[styles.text, styles.text12, styles.textColor919191]}>ИИН</Text>
                                </View>
                                <View style={styles.infoEmployess_val}>
                                    <Text
                                        style={[styles.text, styles.text15, styles.textColor1A1C1E]}> {this.props.employees_select?.iin}</Text>
                                </View>
                            </View>

                            <View style={[styles.infoEmployess, {marginTop: Rem(20)}]}>
                                <View style={styles.infoEmployess_name}>
                                    <Text
                                        style={[styles.text, styles.text12, styles.textColor919191]}>Заработная
                                        плата</Text>
                                </View>
                                <View style={styles.infoEmployess_val}>
                                    <Text
                                        style={[styles.text, styles.text15, styles.textColor1A1C1E]}> {this.props.employees_select?.price}</Text>
                                </View>
                            </View>

                            <TouchableOpacity
                                onPress={() => {
                                    // EmployeesSingleNalog
                                    this.props.navigation.navigate('EmployeesSingleNalog');

                                }}
                                style={[styles.btnCons]}>
                                <Image
                                    style={styles.backImg}
                                    resizeMode="contain"
                                    source={require('../../assets/images/u_receipt.png')}
                                />
                                <Text
                                    style={[styles.text, styles.textBold, styles.text15, styles.textColor1A1C1E, {marginLeft: Rem(10)}]}>
                                    Оплатить налоги
                                </Text>
                                <Image
                                    style={[styles.backImg, {marginLeft: 'auto'}]}
                                    resizeMode="contain"
                                    source={require('../../assets/images/u_angle-right.png')}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {
                                // EmployeesSingleNalog

                            }} style={[styles.btnCons]}>
                                <Image
                                    style={styles.backImg}
                                    resizeMode="contain"
                                    source={require('../../assets/images/u_receipt.png')}
                                />
                                <Text
                                    style={[styles.text, styles.textBold, styles.text15, styles.textColor1A1C1E, {marginLeft: Rem(10)}]}>
                                    Доступ
                                </Text>
                                <Image
                                    style={[styles.backImg, {marginLeft: 'auto'}]}
                                    resizeMode="contain"
                                    source={require('../../assets/images/u_angle-right.png')}
                                />
                            </TouchableOpacity>


                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    productSelect: state.state.productSelect,
    employees: state.state.employees,
    employees_select: state.state.employees_select,
    employees_select_index: state.state.employees_select_index,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
