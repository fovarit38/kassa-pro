import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import State from '../redux/actions/StateActions';


class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Header next={false} name={'Сотрудники'}/>

                        <View style={[styles.containerBottoms, {alignItems: 'center', marginTop: Rem(20)}]}>


                            <AddEmployees/>

                            <View style={styles.evployeesBox}>

                                {
                                    this.props.employees.map((emp, index) => {
                                        return (
                                            <TouchableOpacity onPress={() => {
                                                State({employees_select: emp, employees_select_index: index});
                                                this.props.navigation.navigate('EmployeesSingle');

                                            }} style={styles.evployeeSingle}>
                                                <View style={styles.evployeeSingle_name}>
                                                    <Text
                                                        style={[styles.text, styles.text15, styles.textColor1A1C1E]}>{emp.name}</Text>
                                                    <Image
                                                        style={[styles.backImg]}
                                                        resizeMode="contain"
                                                        source={require('../../assets/images/u_angle-right.png')}
                                                    />

                                                </View>
                                                <View style={styles.evployeeSingle_info}>
                                                    <View style={styles.infoEmployess}>
                                                        <View style={styles.infoEmployess_name}>
                                                            <Text
                                                                style={[styles.text, styles.text12, styles.textColor919191]}>ИИН</Text>
                                                        </View>
                                                        <View style={styles.infoEmployess_val}>
                                                            <Text
                                                                style={[styles.text, styles.text15, styles.textColor1A1C1E]}>{emp.iin}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.infoEmployess}>
                                                        <View style={styles.infoEmployess_name}>
                                                            <Text
                                                                style={[styles.text, styles.text12, styles.textColor919191]}>Заработная
                                                                плата</Text>
                                                        </View>
                                                        <View style={styles.infoEmployess_val}>
                                                            <Text
                                                                style={[styles.text, styles.text15, styles.textColor1A1C1E]}>{emp.price}
                                                                ₸</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        );
                                    })
                                }


                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
