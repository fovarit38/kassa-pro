import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import State from '../redux/actions/StateActions';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import store from '../redux';


class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            ch_select: -1,
            ch_select2: -1,
            ch_select3: -1,
            ch_select4: -1,
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Header next={false} name={'Налоги'}/>

                        <View style={[styles.containerBottoms, {
                            alignItems: 'center',
                        }]}>


                            <View style={[styles.evployeesBox, {
                                justifyContent: 'center',
                                width: '100%',
                                backgroundColor: '#009FF5',
                            }]}>
                                <View style={[styles.container_def]}>
                                    <View style={{paddingVertical: Rem(12)}}>
                                        <Text style={[styles.text, styles.textColor2, styles.text14]}>
                                            1 раздел
                                        </Text>
                                        <Text
                                            style={[styles.text, styles.textColor2, styles.text14, styles.textBold, {marginTop: Rem(5)}]}>
                                            Общая информация о налогоплательщике
                                        </Text>
                                    </View>
                                </View>

                            </View>

                            <View style={{paddingVertical: 22}}>
                                {
                                    [
                                        ['ИИН', 'ХХХХ-ХХХХ-ХХХХ'],
                                        ['ФИО/Наименование налогоплательщика', 'ФИО'],
                                        ['Налоговый период, за который представляется налоговая отчетность:', '2021 г.'],
                                    ].map((item) => {
                                        return (
                                            <Input name={item[0]} place={item[1]} onChangeText={() => {

                                            }}/>
                                        );
                                    })
                                }

                                <View style={{marginVertical: Rem(6), marginBottom: Rem(18)}}>
                                    {
                                        [
                                            ['1-е полугодие', ''],
                                            ['2-е полугодие', ''],
                                        ].map((item, index) => {
                                            return (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setState({ch_select: index});
                                                    }}
                                                    style={[{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                    }, index > 0 && {marginTop: 15}]}>
                                                    <View style={[{
                                                        width: 24,
                                                        height: 24,
                                                        backgroundColor: '#F1F1F1',
                                                        borderRadius: 24,
                                                        marginRight: Rem(15),
                                                        justifyContent: 'center',
                                                        alignItems: 'center',

                                                    }]}>
                                                        {
                                                            this.state.ch_select == index && (
                                                                <View style={[{
                                                                    width: 14,
                                                                    height: 14,
                                                                    backgroundColor: '#009FF5',
                                                                    borderRadius: 24,
                                                                }]}>

                                                                </View>
                                                            )
                                                        }

                                                    </View>
                                                    <Text style={[{
                                                        width: 10,
                                                        flexGrow: 2,
                                                    }, styles.text, styles.text14, styles.textColor1A1C1E]}>
                                                        {item[0]}
                                                    </Text>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                </View>


                                <View style={{marginVertical: Rem(6), marginBottom: Rem(62)}}>
                                    <Text style={[styles.text, styles.text12, {color: '#919191', marginBottom: 5}]}>Отдельные
                                        категории налогоплательщика:</Text>
                                    {
                                        [
                                            ['Доверительный управляющий в соответствии со статьей 40 Налогового кодекса', ''],
                                            ['Учредитель доверительного управления в соответствии со статьей 40 Налогового кодекса', ''],
                                            ['Ведет бухгалтерский учет в соответствии с пунктом 2 статьи 2 Закона Республики Казахстан “О бухгалтерском учете и финансовой отчетности”', ''],
                                            ['Не ведет бухгалтерский учет в соответствии с пунктом 2 статьи 2 Закона Республики Казахстан “О бухгалтерском учете и финансовой отчетности”', ''],
                                        ].map((item, index) => {
                                            return (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setState({ch_select2: index});
                                                    }}
                                                    style={[{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                    }, index > 0 && {marginTop: 15}]}>
                                                    <View style={[{
                                                        width: 24,
                                                        height: 24,
                                                        backgroundColor: '#F1F1F1',
                                                        borderRadius: 24,
                                                        marginRight: Rem(15),
                                                        justifyContent: 'center',
                                                        alignItems: 'center',

                                                    }]}>
                                                        {
                                                            this.state.ch_select2 == index && (
                                                                <View style={[{
                                                                    width: 14,
                                                                    height: 14,
                                                                    backgroundColor: '#009FF5',
                                                                    borderRadius: 24,
                                                                }]}>

                                                                </View>
                                                            )
                                                        }

                                                    </View>
                                                    <Text style={[{
                                                        width: 10,
                                                        flexGrow: 2,
                                                    }, styles.text, styles.text14, styles.textColor1A1C1E]}>
                                                        {item[0]}
                                                    </Text>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                </View>


                                <View style={{marginVertical: Rem(6), marginBottom: Rem(32)}}>
                                    <Text style={[styles.text, styles.text12, {color: '#919191', marginBottom: 5}]}>
                                        Вид декларации:
                                    </Text>
                                    {
                                        [
                                            ['Первоначальная', ''],
                                            ['Очередная', ''],
                                            ['Дополнительная', ''],
                                            ['Дополнительная по уведомлению', ''],
                                            ['Ликвидационная', ''],
                                        ].map((item, index) => {
                                            return (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setState({ch_select3: index});
                                                    }}
                                                    style={[{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                    }, index > 0 && {marginTop: 15}]}>
                                                    <View style={[{
                                                        width: 24,
                                                        height: 24,
                                                        backgroundColor: '#F1F1F1',
                                                        borderRadius: 24,
                                                        marginRight: Rem(15),
                                                        justifyContent: 'center',
                                                        alignItems: 'center',

                                                    }]}>
                                                        {
                                                            this.state.ch_select3 == index && (
                                                                <View style={[{
                                                                    width: 14,
                                                                    height: 14,
                                                                    backgroundColor: '#009FF5',
                                                                    borderRadius: 24,
                                                                }]}>

                                                                </View>
                                                            )
                                                        }

                                                    </View>
                                                    <Text style={[{
                                                        width: 10,
                                                        flexGrow: 2,
                                                    }, styles.text, styles.text14, styles.textColor1A1C1E]}>
                                                        {item[0]}
                                                    </Text>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                </View>


                                {
                                    [
                                        ['Номер уведомления:', 'Введите номер уведомления'],
                                        ['Дата уведомления:', 'ДД.ММ.ГГ'],
                                    ].map((item) => {
                                        return (
                                            <Input name={item[0]} place={item[1]} onChangeText={() => {

                                            }}/>
                                        );
                                    })
                                }


                                <View style={{marginVertical: Rem(6), marginBottom: Rem(32)}}>
                                    <Text style={[styles.text, styles.text12, {color: '#919191', marginBottom: 5}]}>
                                        Вид декларации:
                                    </Text>
                                    {
                                        [
                                            ['Резидент Республики Казахстан', ''],
                                            ['Нерезидент Республики Казахстан', ''],
                                        ].map((item, index) => {
                                            return (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setState({ch_select4: index});
                                                    }}
                                                    style={[{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                    }, index > 0 && {marginTop: 15}]}>
                                                    <View style={[{
                                                        width: 24,
                                                        height: 24,
                                                        backgroundColor: '#F1F1F1',
                                                        borderRadius: 24,
                                                        marginRight: Rem(15),
                                                        justifyContent: 'center',
                                                        alignItems: 'center',

                                                    }]}>
                                                        {
                                                            this.state.ch_select4 == index && (
                                                                <View style={[{
                                                                    width: 14,
                                                                    height: 14,
                                                                    backgroundColor: '#009FF5',
                                                                    borderRadius: 24,
                                                                }]}>

                                                                </View>
                                                            )
                                                        }

                                                    </View>
                                                    <Text style={[{
                                                        width: 10,
                                                        flexGrow: 2,
                                                    }, styles.text, styles.text14, styles.textColor1A1C1E]}>
                                                        {item[0]}
                                                    </Text>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                </View>


                                <Btn click={() => {

                                    this.props.navigation.navigate('Nalog2');

                                }} name={(
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Text style={{color: '#fff'}}>Далее</Text>
                                    </View>
                                )}/>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
