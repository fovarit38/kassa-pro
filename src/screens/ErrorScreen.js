import React, {useEffect} from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';

class HomeScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
        };


    }

    componentDidMount() {

        let get = axios.get(`${API_PATH}/database/get/Catalog`);
        get.then(({data}) => {
            this.setState({catalogs: data.data});
        }).catch(error => {

        });

        let get2 = axios.get(`${API_PATH}/database/get/Restaurant`);
        get2.then(({data}) => {
            this.setState({orders: data.data});
        }).catch(error => {

        });

    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    render() {
        return (
            <>
                <View style={{flexDirection: 'column', flex: 1, height: '100%'}}>
                    <View style={{
                        backgroundColor: 'red',
                        flex: 1,
                        height: 200,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <Text style={{color: '#fff', fontSize: 18, textAlign: 'center'}}>Приложение не прошло проверку
                            целостности и не может корректно работать, ожидайте</Text>

                        <TouchableOpacity
                            onPress={() => {

                            }}
                            style={{
                                width: '80%',
                                paddingTop: 10,
                                paddingBottom: 10,
                                backgroundColor: '#009FF5',
                                marginTop: 60,
                            }}>
                            <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Обновить</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapDispatchToProps = dispatch => (
    bindActionCreators(
        {
            removeError,
        }
        , dispatch)
);

export default connect(null, mapDispatchToProps)(HomeScreen);
