import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    Platform, Alert, ActivityIndicator,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import BarcodeScanner from 'react-native-scan-barcode';

import * as permissions from 'react-native-permissions';
import {request, PERMISSIONS} from 'react-native-permissions';


//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';
import InputSearch from '../components/input/inputSearch';
import Radio from '../components/radio/Radio';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import Rem from '../utils/rem';
import store from '../redux';
import State from '../redux/actions/StateActions';
import Api from '../services';

class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            bearCode: false,
            bearCodeInfo: false,
            setValName: false,
            torchMode: 'off',
            cameraType: 'back',
            codeLocation: '',
            formDataVal: {},
            formData: {
                input: [
                    // {name: 'who', place: 'Выбрать файл', val: 'ЭЦП', type: 'input', secret: false},
                    {name: 'code', place: '', val: 'Штрих код', type: 'input', secret: false, valdef: ''},
                    {
                        name: 'name',
                        place: 'Введите название',
                        val: 'Название товара',
                        type: 'input',
                        secret: false,
                        valdef: '',
                    },
                    {name: 'price', place: '0 ₸', val: 'Цена', type: 'input', secret: false, valdef: ''},
                    {
                        name: 'ed',
                        place: 'Единица измерения ',
                        val: 'Единица измерения',
                        type: 'input',
                        secret: false,
                        valdef: '',
                    },
                    {name: 'count', place: '0 шт', val: 'Количество', type: 'input', secret: false, valdef: ''},
                    {name: 'brand', place: 'BOSCH', val: 'Бренд', type: 'input', secret: false, valdef: ''},
                    {name: 'color', place: 'Черный', val: 'Цвет', type: 'input', secret: false, valdef: ''},
                ],
            },
        };


    }

    barcodeReceived(e) {
        console.log('Barcode: ' + e.data);
        console.log('Type: ' + e.type);
        State({
            strih_code: e.data,
            search_info: true,
        });
        this.setState({bearCode: false});
    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    setModalVisibleScanner(event) {
        this.setState({bearCode: event});

    }


    async getInfoBearCOde() {
        State({search_info: false});
        this.setState({bearCodeInfo: true});
        let infoCode = await Api.getBearCode(this.props.strih_code);


        if (this.state.setValName != false) {
            this.state.setValName(infoCode[0]);
        }
        let preFormDataVal = this.state.formDataVal;
        preFormDataVal['name'] = infoCode[0];
        this.setState({bearCodeInfo: false, formDataVal: preFormDataVal});
    }

    render() {

        if (this.state.codeLocation != this.props.strih_code) {
            let preFormDataVal = this.state.formDataVal;
            preFormDataVal['code'] = this.props.strih_code;
            this.setState({codeLocation: this.props.strih_code, formDataVal: preFormDataVal});
        }
        if (this.props.search_info) {
            this.getInfoBearCOde();
        }
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Modal
                            onSwipeComplete={() => this.setModalVisibleScanner(false)}
                            onBackdropPress={() => this.setModalVisibleScanner(false)}
                            isVisible={this.state.bearCode}
                            style={{margin: 0, justifyContent: 'center', alignItems: 'center'}}
                            swipeDirection="right"
                            propagateSwipe
                            animationIn="slideInRight"
                            animationOut="slideOutRight"

                        >
                            <View style={{flex: 1, backgroundColor: '#fff', width: '100%', position: 'relative'}}>
                                <BarcodeScanner
                                    onBarCodeRead={(e) => {
                                        this.barcodeReceived(e);
                                    }}
                                    style={{flex: 1}}
                                    torchMode={this.state.torchMode}
                                    cameraType={this.state.cameraType}

                                />
                                <TouchableOpacity style={{
                                    position: 'absolute',
                                    zIndex: 10,
                                    width: Rem(207),
                                    height: Rem(36),
                                    bottom: Rem(27),
                                    left: Rem(16),
                                    backgroundColor: '#FFFFFF',
                                    borderRadius: Rem(10),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Text style={[styles.text, styles.text13, {color: '#009FF5'}]}>Ввести штрих код
                                        вручную</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{
                                    position: 'absolute',
                                    zIndex: 10,
                                    top: Rem(27),
                                    right: Rem(16),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                                                  onPress={() => {
                                                      this.setState({
                                                          bearCode: false,
                                                      });
                                                  }}
                                >
                                    <Image
                                        style={styles.backImg}
                                        resizeMode="contain"
                                        source={require('../../assets/images/fi_x.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                        </Modal>


                        <Modal
                            isVisible={this.state.bearCodeInfo}
                            style={{margin: 0, justifyContent: 'center', alignItems: 'center'}}
                            animationIn="slideInRight"
                            animationOut="slideOutRight"
                        >
                            <View style={{
                                flex: 1,
                                width: '100%',
                                position: 'relative',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                <Text style={[styles.text, styles.text18, styles.textColor2, {marginBottom: 24}]}>
                                    Получение информации о штрих коде
                                </Text>
                                <ActivityIndicator size="small" color="#fff"/>
                            </View>
                        </Modal>


                        <Header scanner={() => {
                            console.log('save');
                            this.setState({
                                bearCode: true,
                            });
                        }} next={false} name={'Добавить товар'}/>

                        <View style={styles.containerBottoms}>
                            <View style={[styles.fullBox, styles.mt20]}>

                                {
                                    this.state.formData.input.map((input, index) => {

                                            if (input.name == 'code') {
                                                return (
                                                    <View style={{position: 'relative'}}>
                                                        <View style={[styles.inputTel]}>
                                                            <Text style={[styles.text, styles.text12, {
                                                                color: '#919191',
                                                                marginBottom: 5,
                                                            }]}>{input.name}</Text>
                                                            <View>
                                                                <TextInput
                                                                    onChangeText={(e) => {
                                                                        let preFormDataVal = this.state.formDataVal;
                                                                        preFormDataVal[input.name] = e;
                                                                        this.setState({formDataVal: preFormDataVal});
                                                                    }}
                                                                    value={this.state.formDataVal?.code}
                                                                    placeholder={input.place}
                                                                    style={[styles.phoneInput, styles.text, styles.text16]}
                                                                />

                                                            </View>
                                                        </View>

                                                        {input.name == 'code' &&
                                                        (
                                                            <TouchableOpacity
                                                                onPress={() => {
                                                                    request(Platform.OS === 'ios' ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA).then((result) => {
                                                                        // setPermissionResult(result)
                                                                        if (result == 'granted') {
                                                                            this.setModalVisibleScanner(true);
                                                                        } else {
                                                                            Alert.alert('Нет доступа к камере');
                                                                        }
                                                                    });
                                                                }} style={{
                                                                position: 'absolute',
                                                                right: Rem(11),
                                                                bottom: Rem(26),
                                                            }}>
                                                                <Image
                                                                    style={styles.backImg}
                                                                    resizeMode="contain"
                                                                    source={require('../../assets/images/u_barcode-scan.png')}
                                                                />
                                                            </TouchableOpacity>
                                                        )
                                                        }
                                                    </View>
                                                );
                                            } else if (input.type == 'input') {
                                                return (
                                                    <View style={{position: 'relative'}}>
                                                        <Input
                                                            returnSetVal={(setVal, name) => {
                                                                if (name == 'Название товара') {
                                                                    if (this.state.setValName == false) {
                                                                        this.setState({setValName: setVal});
                                                                    }
                                                                }
                                                            }
                                                            }
                                                            name={input.val} place={input.place} secret={input.secret}
                                                            value={input.name == 'code' ? this.state.codeLocation : this.state.formDataVal[input.name]}
                                                            onChangeText={(e) => {
                                                                let preFormDataVal = this.state.formDataVal;
                                                                preFormDataVal[input.name] = e;
                                                                this.setState({formDataVal: preFormDataVal});
                                                            }}/>

                                                        {input.name == 'code' &&
                                                        (
                                                            <TouchableOpacity
                                                                onPress={() => {

                                                                    request(Platform.OS === 'ios' ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA).then((result) => {
                                                                        // setPermissionResult(result)
                                                                        if (result == 'granted') {
                                                                            this.setModalVisibleScanner(true);
                                                                        } else {
                                                                            Alert.alert('Нет доступа к камере');
                                                                        }
                                                                    });

                                                                }} style={{
                                                                position: 'absolute',
                                                                right: Rem(11),
                                                                bottom: Rem(26),
                                                            }}>
                                                                <Image
                                                                    style={styles.backImg}
                                                                    resizeMode="contain"
                                                                    source={require('../../assets/images/u_barcode-scan.png')}
                                                                />
                                                            </TouchableOpacity>
                                                        )
                                                        }
                                                    </View>
                                                );
                                            }

                                            return null;

                                        },
                                    )
                                }

                                <Btn click={() => {

                                    let insfo = store.getState().state.products;

                                    insfo.push({
                                        code: this.state.formDataVal?.code,
                                        count: this.state.formDataVal?.count,
                                        price: this.state.formDataVal?.price,
                                        ed: this.state.formDataVal?.ed,
                                        name: this.state.formDataVal?.name,
                                        brand: this.state.formDataVal?.brand,
                                        color: this.state.formDataVal?.color,
                                    });
                                    // setModalVisibleMessage(false);
                                    this.props.navigation.navigate('Storage');
                                    State({products: insfo});
                                }} name={(
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Text style={{color: '#fff'}}>Добавить товар</Text>
                                    </View>
                                )}/>

                            </View>


                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
    strih_code: state.state.strih_code,
    search_info: state.state.search_info,
});

export default connect(mapStateToProps, null)(StorageScreen);
