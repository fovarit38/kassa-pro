import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator, Alert,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import State from '../redux/actions/StateActions';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import store from '../redux';


class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            ch_select: -1,
            ch_select2: -1,
            ch_select3: -1,
            ch_select4: -1,
            gitivo: 0,
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Header next={false} name={'Налоги'}/>

                        <View style={[styles.containerBottoms, {
                            alignItems: 'center',
                        }]}>


                            <View style={[styles.evployeesBox, {
                                justifyContent: 'center',
                                width: '100%',
                                backgroundColor: '#009FF5',
                            }]}>
                                <View style={[styles.container_def]}>
                                    <View style={{paddingVertical: Rem(12)}}>
                                        <Text style={[styles.text, styles.textColor2, styles.text14]}>
                                            7 раздел
                                        </Text>
                                        <Text
                                            style={[styles.text, styles.textColor2, styles.text14, styles.textBold, {marginTop: Rem(5)}]}>
                                            Ответственность налогоплательщика
                                        </Text>
                                    </View>
                                </View>

                            </View>

                            <View style={{paddingVertical: 22}}>
                                {
                                    [
                                        ['ФИО налогоплательщика(руководителя):', 'Арманов Ильяс Сапарулы'],
                                        ['Дата подачи декларации:', 'ДД.ММ.ГГ'],
                                        ['Код органа государственных доходов по месту нахождения:', '0000'],
                                        ['Код органа государственных доходов по месту жительства:', '0000'],
                                    ].map((item) => {
                                        return (
                                            <Input name={item[0]} place={item[1]} onChangeText={() => {

                                            }}/>
                                        );
                                    })
                                }

                                <View style={{marginTop:Rem(12)}}>
                                    <Btn click={() => {

                                        this.setState({gitivo: 1});
                                        setTimeout(() => {
                                            this.setState({gitivo: 0});
                                            this.props.navigation.navigate('Home');
                                            Alert.alert('Отчет отправлен');
                                        }, 2000);

                                    }} name={(
                                        <View style={{flexDirection: 'row', alignItems: 'center'}}>


                                            {this.state.gitivo == 1 ? (
                                                <ActivityIndicator size="small" color="#fff"/>) : (
                                                <Text style={{color: '#fff'}}>Готово</Text>)}
                                        </View>
                                    )}/>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
