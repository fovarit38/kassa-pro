import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import State from '../redux/actions/StateActions';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import store from '../redux';


class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            ch_select: -1,
            ch_select2: -1,
            ch_select3: -1,
            ch_select4: -1,
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Header next={false} name={'Налоги'}/>

                        <View style={[styles.containerBottoms, {
                            alignItems: 'center',
                        }]}>


                            <View style={[styles.evployeesBox, {
                                justifyContent: 'center',
                                width: '100%',
                                backgroundColor: '#009FF5',
                            }]}>
                                <View style={[styles.container_def]}>
                                    <View style={{paddingVertical: Rem(12)}}>
                                        <Text style={[styles.text, styles.textColor2, styles.text14]}>
                                            2 раздел
                                        </Text>
                                        <Text
                                            style={[styles.text, styles.textColor2, styles.text14, styles.textBold, {marginTop: Rem(5)}]}>
                                            Исчисление налогов
                                        </Text>
                                    </View>
                                </View>

                            </View>

                            <View style={{paddingVertical: 22}}>
                                {
                                    [
                                        ['910.00.001 Доход:', '0 ₸'],
                                        ['Доходы, полученные путем безналичных расчетов:', '0 ₸'],
                                        ['В том числе, с применением трехкомпонентной системы:', '0 ₸'],
                                        ['Доходы, полученные путем наличных расчетов:', '0 ₸'],
                                        ['В том числе, с применением трехкомпонентной системы:', '0 ₸'],
                                        ['910.00.002 В том числе доход от корректировки в соответствии с Законом о трансфертном ценообразовании:', '0 ₸'],
                                        ['910.00.003 Среднесписочная численность работников, в том числе:', '0 ₸'],
                                        ['Пенсионеры:', '0 ₸'],
                                        ['Инвалиды:', '0 ₸'],
                                        ['910.00.004 Среднемесячная заработная плата на одного работника:', '0 ₸'],
                                        ['910.00.005 Сумма исчисленных налогов (910.00.001 х 3%):', '0 ₸'],
                                        ['910.00.006 Корректировка суммы налогов в соответствии с пунктом 2 статьи 687 Налогового кодекса:', '0 ₸'],
                                        ['910.00.007 Сумма исчисленных налогов (910.00.001 х 3%):', '0 ₸'],
                                        ['910.00.008 Сумма индивидуального (корпоративного) подоходного налога, подлежащего уплате в бюджет (910.00.007 х 0,5):', '0 ₸'],
                                        ['910.00.009 Сумма социального налога, подлежащего уплате в бюджет \n' +
                                        '((910.00.007 x 0,5) - 910.00.013 VII - 910.00.020 VII):', '0 ₸'],
                                    ].map((item) => {
                                        return (
                                            <Input name={item[0]} place={item[1]} onChangeText={() => {

                                            }}/>
                                        );
                                    })
                                }


                                <Btn click={() => {

                                    this.props.navigation.navigate('Nalog3');

                                }} name={(
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Text style={{color: '#fff'}}>Далее</Text>
                                    </View>
                                )}/>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
