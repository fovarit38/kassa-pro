import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import State from '../redux/actions/StateActions';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import store from '../redux';


class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            ch_select: -1,
            ch_select2: -1,
            ch_select3: -1,
            ch_select4: -1,
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Header next={false} name={'Налоги'}/>

                        <View style={[styles.containerBottoms, {
                            alignItems: 'center',
                        }]}>


                            <View style={[styles.evployeesBox, {
                                justifyContent: 'center',
                                width: '100%',
                                backgroundColor: '#009FF5',
                            }]}>
                                <View style={[styles.container_def]}>
                                    <View style={{paddingVertical: Rem(12)}}>
                                        <Text style={[styles.text, styles.textColor2, styles.text14]}>
                                            4 раздел
                                        </Text>
                                        <Text
                                            style={[styles.text, styles.textColor2, styles.text14, styles.textBold, {marginTop: Rem(5)}]}>
                                            Исчисление индивидуального подоходног..
                                        </Text>
                                    </View>
                                </View>

                            </View>

                            <View style={{paddingVertical: 22}}>
                                <Text
                                    style={[styles.text, styles.text14, styles.textBold, styles.textColor1A1C1E, {marginBottom: Rem(18)}]}>
                                    910.00.017. Сумма индивидуального подоходного налога, подлежащая перечислению в бюджет с доходов граждан Республики Казахстан:
                                </Text>
                                {
                                    [
                                        ['1 месяц:', '0 ₸'],
                                        ['2 месяц:', '0 ₸'],
                                        ['3 месяц:', '0 ₸'],
                                        ['4 месяц:', '0 ₸'],
                                        ['5 месяц:', '0 ₸'],
                                        ['6 месяц:', '0 ₸'],
                                        ['Итого за полугодие:', '0 ₸'],
                                    ].map((item) => {
                                        return (
                                            <Input name={item[0]} place={item[1]} onChangeText={() => {

                                            }}/>
                                        );
                                    })
                                }


                                <Btn click={() => {

                                    this.props.navigation.navigate('Nalog5');

                                }} name={(
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Text style={{color: '#fff'}}>Далее</Text>
                                    </View>
                                )}/>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
