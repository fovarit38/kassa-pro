import React, {useEffect} from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity, TextInput} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import BarcodeScanner from 'react-native-scan-barcode';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';
import InputSearch from '../components/input/inputSearch';
import Radio from '../components/radio/Radio';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import Rem from '../utils/rem';
import store from '../redux';
import State from '../redux/actions/StateActions';

class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            bearCode: false,
            torchMode: 'off',
            cameraType: 'back',
            codeLocation: '',
            formDataVal: {},
            formData: {
                input: [
                    // {name: 'who', place: 'Выбрать файл', val: 'ЭЦП', type: 'input', secret: false},
                    {name: 'code', place: '', val: 'Штрих код', type: 'input', secret: false, valdef: ''},
                    {
                        name: 'name',
                        place: 'Введите название',
                        val: 'Название товара',
                        type: 'input',
                        secret: false,
                        valdef: '',
                    },
                    {name: 'price', place: '0 ₸', val: 'Цена', type: 'input', secret: false, valdef: ''},
                    {
                        name: 'ed',
                        place: 'Единица измерения ',
                        val: 'Единица измерения',
                        type: 'input',
                        secret: false,
                        valdef: '',
                    },
                    {name: 'count', place: '0 шт', val: 'Количество', type: 'input', secret: false, valdef: ''},
                    {name: 'brand', place: 'BOSCH', val: 'Бренд', type: 'input', secret: false, valdef: ''},
                    {name: 'color', place: 'Черный', val: 'Цвет', type: 'input', secret: false, valdef: ''},
                ],
            },
        };


    }

    barcodeReceived(e) {

        State({
            strih_code: e.data,
            search_info: false,
        });
        this.setState({bearCode: false});
    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    setModalVisibleScanner(event) {
        this.setState({bearCode: event});

    }

    render() {

        if (this.state.codeLocation != this.props.strih_code) {
            let preFormDataVal = this.state.formDataVal;
            preFormDataVal['code'] = this.props.strih_code;
            this.setState({codeLocation: this.props.strih_code, formDataVal: preFormDataVal});
        }
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>
                        <Header scanner={() => {
                        }} next={false} name={''}/>

                        <View style={styles.containerBottoms}>
                            <View style={[styles.fullBox]}>
                                {
                                    this.state.formData.input.map((input, index) => {
                                            //
                                            return (
                                                <TouchableOpacity onPress={() => {
                                                    // this.props.navigation.navigate('StorageProductEdit');
                                                }} style={[styles.infoEmployess, {
                                                    marginTop: Rem(20),
                                                    borderBottomWidth: 1,
                                                    width: '100%',
                                                    paddingBottom: Rem(15),
                                                    borderBottomColor: '#F1F1F1',
                                                }]}>
                                                    <View style={styles.infoEmployess_name}>
                                                        <Text
                                                            style={[styles.text, styles.text12, styles.textColor919191]}>
                                                            {input.val}:
                                                        </Text>
                                                    </View>
                                                    <View style={styles.infoEmployess_val}>
                                                        <Text
                                                            style={[styles.text, styles.text15, styles.textColor1A1C1E]}> {this.props.productSelect[input.name]}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            );

                                        },
                                    )
                                }


                            </View>


                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
    strih_code: state.state.strih_code,
    productSelect: state.state.productSelect,
});

export default connect(mapStateToProps, null)(StorageScreen);
