import React, {useEffect} from 'react';
import {View, ScrollView, StyleSheet, SafeAreaView, Text, Image, TouchableOpacity} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';

class HomeScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            error: false,
            catalogs: [],
            orders: [],
        };


    }

    componentDidMount() {

        let get = axios.get(`${API_PATH}/database/get/Catalog`);
        get.then(({data}) => {
            this.setState({catalogs: data.data});
        }).catch(error => {

        });

        let get2 = axios.get(`${API_PATH}/database/get/Restaurant`);
        get2.then(({data}) => {
            this.setState({orders: data.data});
        }).catch(error => {

        });

    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    setModalVisibleMessageError(event) {
        this.setState({error: event});
    }

    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Modal
                            onSwipeComplete={() => this.setModalVisibleMessageError(false)}
                            onBackdropPress={() => this.setModalVisibleMessageError(false)}
                            isVisible={this.state.error}
                            style={{margin: 0, justifyContent: 'center', alignItems: 'center', width: '100%'}}
                            swipeDirection="down"
                            propagateSwipe
                            animationIn="slideInUp"
                            animationOut="slideOutDown"
                        >

                            <View style={{flexDirection: 'column', flex: 1, height: '100%'}}>
                                <View style={{
                                    flex: 1,
                                    height: 200,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Text style={{color: '#fff', fontSize: 18, textAlign: 'center'}}>
                                        Данный раздел заблокирован
                                    </Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setModalVisibleMessageError(false)
                                        }}
                                        style={{
                                            width: '80%',
                                            paddingTop: 10,
                                            paddingBottom: 10,
                                            backgroundColor: 'red',
                                            marginTop: 60,
                                        }}>
                                        <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Обновить</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>

                        <Modal
                            onSwipeComplete={() => this.setModalVisibleMessage(false)}
                            onBackdropPress={() => this.setModalVisibleMessage(false)}
                            isVisible={this.state.model}
                            style={{margin: 0, justifyContent: 'center', alignItems: 'center'}}
                            swipeDirection="down"
                            propagateSwipe
                            animationIn="slideInUp"
                            animationOut="slideOutDown"
                        >

                            <View style={styles.modelAlert}>
                                <View style={styles.bodyModel}>
                                    <Text style={[styles.text, styles.text17, styles.textBold]}>Cкоро будет</Text>
                                    <Text style={[styles.text, styles.text13, {
                                        textAlign: 'center',
                                        marginTop: 10,
                                        color: '#1A1C1E',
                                    }]}>
                                        Эти страницы находятся в {'\n'} разработке, в
                                        скором времени доступ {'\n'} к этим
                                        страницам откроется
                                    </Text>
                                </View>


                                <TouchableOpacity
                                    onPress={() => {
                                        this.setModalVisibleMessage(false);
                                    }}
                                    style={styles.btnAlert}>

                                    <Text style={[styles.text, styles.text13, styles.textColor2]}>Понятно!</Text>

                                </TouchableOpacity>
                            </View>
                        </Modal>

                        <View style={styles.containerBottoms}>
                            <View style={[styles.fullBox, styles.mt20]}>
                                <Text style={[styles.text, styles.textBold, styles.text34]}>
                                    Главная
                                </Text>

                                <View style={styles.catalogBox}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate('Employees');
                                        }}
                                        style={styles.btnNavigation}>

                                        <Image
                                            style={styles.backImgCntr}
                                            resizeMode="cover"
                                            source={require('../../assets/images/staff.png')}
                                        />

                                        <Text
                                            style={[styles.text, styles.textColor3, styles.textBold, styles.text17]}>Сотрудники</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate('Nalog');
                                        }}
                                        style={styles.btnNavigation}>

                                        <Image
                                            style={styles.backImgCntr}
                                            resizeMode="cover"
                                            source={require('../../assets/images/taxes.png')}
                                        />
                                        <Text
                                            style={[styles.text, styles.textColor3, styles.textBold, styles.text17]}>Налоги</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate('Storage');
                                        }}
                                        style={[styles.btnNavigation, styles.btnNavigationW3]}>
                                        <Image
                                            style={styles.backImgCntr}
                                            resizeMode="cover"
                                            source={require('../../assets/images/virtual-warehouse.png')}
                                        />
                                        <Text
                                            style={[styles.text, styles.textColor3, styles.textBold, styles.text17]}>
                                            Виртуальный склад</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                            <View>
                                <View style={[styles.fullBox, styles.mt20, {
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                }]}>
                                    <Text style={[styles.text, styles.textBold, styles.text17]}>
                                        Новости
                                    </Text>
                                    <Text style={[styles.text, styles.text13, styles.textBold, styles.textColor1]}>
                                        Все
                                        <Image
                                            resizeMode="cover"
                                            style={{width: 24, height: 24, position: 'absolute', right: 0}}
                                            source={require('../../assets/images/right.png')}
                                        />
                                    </Text>
                                </View>

                                <ScrollView style={styles.mt20} showsHorizontalScrollIndicator={false}
                                            horizontal={true}>
                                    <View style={styles.banner}>
                                        <Image
                                            style={styles.fullIm}
                                            resizeMode="cover"
                                            source={require('../../assets/images/banner.png')}
                                        />
                                        <Text
                                            style={[styles.text, styles.textBold, styles.text13, styles.textColor2, styles.textBottom]}>
                                            Lorem ipsum dolor sit amet, consectetur
                                        </Text>
                                    </View>

                                    <View style={styles.banner}>
                                        <Image
                                            style={styles.fullIm}
                                            resizeMode="cover"
                                            source={require('../../assets/images/banner.png')}
                                        />
                                        <Text
                                            style={[styles.text, styles.textBold, styles.text13, styles.textColor2, styles.textBottom]}>
                                            Lorem ipsum dolor sit amet, consectetur
                                        </Text>
                                    </View>

                                    <View style={[styles.banner, {marginRight: 15}]}>
                                        <Image
                                            style={styles.fullIm}
                                            resizeMode="cover"
                                            source={require('../../assets/images/banner.png')}
                                        />
                                        <Text
                                            style={[styles.text, styles.textBold, styles.text13, styles.textColor2, styles.textBottom]}>
                                            Lorem ipsum dolor sit amet, consectetur
                                        </Text>
                                    </View>

                                </ScrollView>
                            </View>
                            <View>
                                <View style={[styles.fullBox, styles.mt20]}>
                                    <Text style={[styles.text, styles.textBold, styles.text17]}>
                                        FAQ
                                    </Text>
                                </View>
                                <View style={styles.mt20}>
                                    <View style={[styles.center, {justifyContent: 'center', alignItems: 'center'}]}>
                                        <View style={styles.faqControl}>
                                            <Text style={[styles.text, styles.text15]}>Tellus orci ac auctor augue
                                                mauris?</Text>
                                            <Image
                                                resizeMode="cover"
                                                style={{width: 24, height: 24, position: 'absolute', right: 10}}
                                                source={require('../../assets/images/grayRight.png')}
                                            />
                                        </View>
                                        <View style={[styles.faqControl, styles.mt20]}>
                                            <Text style={[styles.text, styles.text15]}>Euismod in pellentesque
                                                massa?</Text>
                                            <Image
                                                resizeMode="cover"
                                                style={{width: 24, height: 24, position: 'absolute', right: 10}}
                                                source={require('../../assets/images/grayRight.png')}
                                            />
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapDispatchToProps = dispatch => (
    bindActionCreators(
        {
            removeError,
        }
        , dispatch)
);

export default connect(null, mapDispatchToProps)(HomeScreen);
