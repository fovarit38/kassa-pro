import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator, Alert,
} from 'react-native';

import DatePicker from 'react-native-date-picker';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';
import InputSearch from '../components/input/inputSearch';
import Btn from '../components/btn/btn';
import {style} from 'redux-logger/src/diff';
import {request} from 'react-native-permissions';
import store from '../redux';
import State from '../redux/actions/StateActions';

class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
            date: new Date(),
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    setDate(date) {
        this.setState({date: date});
    }

    render() {
        let motch = [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь',
        ];

        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>
                        <Header icon={4} next={false} name={'Обязательные пенсионные вз...'}/>
                        <View style={[styles.container_def, styles.containerBottoms, {marginTop: Rem(0)}]}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: Rem(20)}}>
                                <Text style={[styles.text, styles.text15, styles.textColor1A1C1E]}>Период оплаты</Text>
                                <Text
                                    style={[styles.text, styles.text15, styles.textColor1]}>{motch[this.state.date.getMonth()]} {this.state.date.getFullYear()}</Text>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                <DatePicker
                                    date={this.state.date}
                                    mode="date"
                                    style={{marginTop: Rem(18)}}
                                    onDateChange={(e) => {
                                        this.setDate(new Date(e));
                                    }}
                                />
                            </View>

                            <View style={{marginTop: Rem(15)}}>
                                {
                                    [
                                        ['ИИН плательщика', '2911-8238-5932'],
                                        ['ИИН работника', this.props.employees_select?.iin],
                                        ['Сумма оплаты', parseInt(this.props.employees_select?.price * 0.2)],
                                    ].map((item) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => {
                                                    // Alert.alert("ошибка интеграции вы не можете сдать "+item)

                                                }}
                                                style={[styles.btnCons, styles.btnConsBig, {justifyContent: 'space-between'}]}>
                                                <Text
                                                    style={[styles.text, styles.text15, styles.textColor1A1C1E, {}]}>
                                                    {item[0]}
                                                </Text>
                                                <Text
                                                    style={[styles.text, styles.text15, styles.textBold, styles.textColor1A1C1E, {}]}>
                                                    {item[1]}
                                                </Text>
                                            </TouchableOpacity>
                                        );
                                    })
                                }
                            </View>
                            <View style={{marginTop: Rem(38)}}>
                                <Btn click={() => {

                                }} name={(
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Text style={{color: '#fff'}}>Оплатить</Text>
                                    </View>
                                )}/>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
    employees_select: state.state.employees_select,
    employees_select_index: state.state.employees_select_index,
});

export default connect(mapStateToProps, null)(StorageScreen);
