import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  Platform,
} from 'react-native';

// Redux
import {connect} from 'react-redux';

// Components
import Header from '../components/general/Header';
import BarcodeScanner from 'react-native-scan-barcode';

import * as permissions from 'react-native-permissions';
import {request, PERMISSIONS} from 'react-native-permissions';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';
import InputSearch from '../components/input/inputSearch';
import Radio from '../components/radio/Radio';
import Input from '../components/input/input';
import Btn from '../components/btn/btn';
import Rem from '../utils/rem';
import store from '../redux';
import State from '../redux/actions/StateActions';
import {useNavigation} from '@react-navigation/native';
import {
  Camera,
  useCameraDevices,
  useFrameProcessor,
} from 'react-native-vision-camera';
import 'react-native-reanimated'

const StorageScreen = ({products}) => {
  const [torchMode, setTorchMode] = useState('off');
  const [cameraType, setCameraType] = useState('off');
  const [bearCode, setBearCode] = useState(false);
  const {navigate} = useNavigation();

  const devices = useCameraDevices();
  const device = devices.back;
  const frameProcessor = useFrameProcessor(frame => {
    'worklet'
    // const qrCodes = scanQRCodes(frame)
    console.log(`QR Codes in Frame: ${frame}`);
  }, []);
  // constructor();
  // {
  //     super();
  //     this.state = {
  //         model: false,
  //         bearCode: false,
  //         torchMode: 'off',
  //         cameraType: 'back',
  //     };
  //
  //
  // }

  function barcodeReceived(e) {
    State({
      strih_code: e.data,
      search_info: true,
    });

    navigate('StorageProduct');
    setBearCode(false);
  }

  function componentDidMount() {}

  function setModalVisibleScanner(event) {
    setBearCode(event);
    navigate('StorageProduct');
  }

  return (
    <>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Modal
            onSwipeComplete={() => setModalVisibleScanner(false)}
            onBackdropPress={() => setModalVisibleScanner(false)}
            isVisible={bearCode}
            style={{margin: 0, justifyContent: 'center', alignItems: 'center'}}
            swipeDirection="right"
            propagateSwipe
            animationIn="slideInRight"
            animationOut="slideOutRight">
            <View
              style={{
                flex: 1,
                backgroundColor: '#fff',
                width: '100%',
                position: 'relative',
              }}>
              {device ? (
                <Camera
                  frameProcessor={frameProcessor}
                  style={StyleSheet.absoluteFill}
                  device={device}
                  isActive={true}
                />
              ) : (
                <></>
              )}
              {/*<BarcodeScanner*/}
              {/*  onBarCodeRead={e => {*/}
              {/*    barcodeReceived(e);*/}
              {/*  }}*/}
              {/*  style={{flex: 1}}*/}
              {/*  torchMode={torchMode}*/}
              {/*  cameraType={cameraType}*/}
              {/*/>*/}
              <TouchableOpacity
                onPress={() => {
                  navigate('StorageProduct');
                }}
                style={{
                  position: 'absolute',
                  zIndex: 10,
                  width: Rem(207),
                  height: Rem(36),
                  bottom: Rem(27),
                  left: Rem(16),
                  backgroundColor: '#FFFFFF',
                  borderRadius: Rem(10),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={[styles.text, styles.text13, {color: '#009FF5'}]}>
                  Ввести штрих код вручную
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  zIndex: 10,
                  top: Rem(27),
                  right: Rem(16),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => {
                  setBearCode(false);
                }}>
                <Image
                  style={styles.backImg}
                  resizeMode="contain"
                  source={require('../../assets/images/fi_x.png')}
                />
              </TouchableOpacity>
            </View>
          </Modal>
          <Header
            scanner={() => {
              request(
                Platform.OS === 'ios'
                  ? PERMISSIONS.IOS.CAMERA
                  : PERMISSIONS.ANDROID.CAMERA,
              ).then(result => {
                // setPermissionResult(result)
                if (result === 'granted') {
                  setBearCode(true);
                } else {
                  Alert.alert('Нет доступа к камере');
                }
              });
            }}
            name={'Виртуальный склад'}
          />

          <View style={styles.containerBottoms}>
            <View style={[styles.fullBox, styles.mt20]}>
              <InputSearch place={'Поиск'} onChangeText={() => {}} />

              <View style={styles.evployeesBox}>
                {products.map((emp, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        State({productSelect: emp, productSelect_index: index});
                        navigate('StorageProductSelect');
                      }}
                      style={styles.evployeeSingle}>
                      <View style={styles.evployeeSingle_name}>
                        <Text
                          style={[
                            styles.text,
                            styles.text15,
                            styles.textColor1A1C1E,
                          ]}>
                          {emp.name}
                        </Text>
                        <Image
                          style={[styles.backImg]}
                          resizeMode="contain"
                          source={require('../../assets/images/u_angle-right.png')}
                        />
                      </View>
                      <View style={styles.evployeeSingle_info}>
                        <View
                          style={[
                            styles.infoEmployessmini,
                            {
                              paddingHorizontal: Rem(15),
                              paddingLeft: 0,
                              borderRightWidth: 1,
                              borderColor: '#EFEFEF',
                            },
                          ]}>
                          <View style={styles.infoEmployess_val}>
                            <Text
                              style={[
                                styles.text,
                                styles.text15,
                                styles.textColor1A1C1E,
                              ]}>
                              {emp.price} ₸
                            </Text>
                          </View>
                        </View>
                        <View
                          style={[
                            styles.infoEmployessmini,
                            {
                              paddingHorizontal: Rem(15),
                              borderRightWidth: 1,
                              borderColor: '#EFEFEF',
                            },
                          ]}>
                          <View style={styles.infoEmployess_val}>
                            <Text
                              style={[
                                styles.text,
                                styles.text15,
                                styles.textColor1A1C1E,
                              ]}>
                              {emp.count} {emp.ed}
                            </Text>
                          </View>
                        </View>
                        <View
                          style={[
                            styles.infoEmployessmini,
                            {paddingHorizontal: Rem(15)},
                          ]}>
                          <View style={[styles.infoEmployess_val]}>
                            <Text
                              style={[
                                styles.text,
                                styles.text15,
                                styles.textColor1A1C1E,
                              ]}>
                              {emp.code}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  ...{
    container: {
      flex: 1,
    },
    scrollView: {
      paddingHorizontal: 16,
    },
  },
  ...Css(),
});

const mapStateToProps = state => ({
  selected_product: state.state.selected_product,
  employees: state.state.employees,
  products: state.state.products,
  state_info: state.state,
  is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
