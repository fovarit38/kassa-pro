import React, {useState} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Alert,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions

// Components
import Input from '../../components/input/input';
import Btn from '../../components/btn/btn';
import Radio from '../../components/radio/Radio';

import Back from '../../components/btn/Back';


import Css from '../../utils/css';
import {API_PATH} from '../../../config';
import {useNavigation} from '@react-navigation/native';
import {SvgUri} from 'react-native-svg';


import store from '../../redux';
import State from '../../redux/actions/StateActions';

const RegistrationScreen = (props) => {
    const navigation = useNavigation();
    const [code, setCode] = useState(1);
    const [formDataVal, setFormData] = useState({});

    const [formActive, setFormActive] = useState(0);


    let formData = [
        {
            name: 'Кто вы?',
            input: [
                {name: 'who', val: 'Я владелец ИП', type: 'radio'},
                {name: 'who', val: 'Я сотрудник', type: 'radio'},
            ],
        },
        {
            name: 'Регистрация владельца ИП',
            input: [
                // {name: 'who', place: 'Выбрать файл', val: 'ЭЦП', type: 'input', secret: false},
                {name: 'who', place: 'kassapro@info.com', val: 'E-mail', type: 'input', secret: false},
                {name: 'who', place: '', val: 'Код органа гос. доходов', type: 'input', secret: false},
                {
                    name: 'who',
                    place: '',
                    val: 'Код органа гос. доходов по месту жительства',
                    type: 'input',
                    secret: false,
                },
            ],
        },
        {
            name: 'Регистрация владельца ИП',
            input: [
                {name: 'who', place: 'Придумайте пароль', val: 'Пароль', type: 'input', secret: false},
                {name: 'who', place: 'Повторите пароль', val: 'Повторите пароль', type: 'input', secret: false},
            ],
        },
    ];

    let preFormDataVal = formDataVal;


    return (
        <>
            <SafeAreaView style={[styles.container, styles.containerPaddingTop]}>
                <ScrollView>
                    <View style={[styles.center, {marginBottom: 100}]}>
                        <View style={styles.fullBox}>
                            <Back
                                click={() => {
                                    if (formActive > 0) {
                                        setFormActive(formActive - 1);
                                    } else {
                                        navigation.goBack();
                                    }
                                }}
                            />
                            <Text
                                style={[styles.autTitle, styles.text, styles.textBold, styles.text20, {marginLeft: -4}]}> {formData[formActive].name} </Text>
                        </View>

                        {
                            formData[formActive].input.map((input, index) => {
                                    if (input.type == 'radio') {
                                        return (
                                            <Radio click={() => {
                                                preFormDataVal[input.name] = input.val;

                                                setFormData(preFormDataVal);
                                                setCode(code + 1);
                                            }}
                                                   active={typeof formDataVal[input.name] != 'undefined' && formDataVal[input.name] == input.val}
                                                   name={input.val}/>
                                        );
                                    }
                                    if (input.type == 'input') {
                                        return (
                                            <Input name={input.val} place={input.place} secret={input.secret}
                                                   onChangeText={() => {

                                                   }}/>
                                        );
                                    }

                                    return null;

                                },
                            )
                        }


                        <Btn name={formData.length - 1 == formActive ? 'Зарегистрироваться' : 'Далее'} click={() => {
                            if (formData.length - 1 > formActive) {
                                setFormActive(formActive + 1);
                            } else {
                                State({tabBar: true});
                                navigation.navigate('Home');
                            }
                        }}
                        />
                        {formData.length - 1 != formActive && (
                            <>
                                <View style={[styles.fullBox, styles.mt20]}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            navigation.navigate('Auth');
                                        }}
                                    >
                                        <Text>
                                            <Text style={[styles.text, styles.textColor3, styles.text15]}>У вас уже есть
                                                аккаунт? </Text>
                                            <Text style={[styles.text, styles.textColor1, styles.text15]}>Войти</Text>
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.fullBox, styles.mt20]}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            State({tabBar: true});
                                            navigation.navigate('Home');
                                        }}
                                    >
                                        <Text>
                                            <Text style={[styles.text, styles.textColor3, styles.text15]}>Хотите узнать
                                                о
                                                приложении
                                                подробнее? </Text>
                                            <Text style={[styles.text, styles.textColor1, styles.text15]}>Демо
                                                версия</Text>
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </>
                        )
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    )
        ;
};


const styles = StyleSheet.create(
    {
        ...{
            container: {
                flex: 1,
            },
            center: {
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-start',
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
        ...Css(),
    },
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps)(RegistrationScreen);
