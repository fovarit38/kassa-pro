import React, {useState, useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Alert,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions

// Components
import Input from '../../components/input/input';
import Btn from '../../components/btn/btn';



import Css from '../../utils/css';
import {API_PATH} from '../../../config';
import {useNavigation} from '@react-navigation/native';
import {SvgUri} from 'react-native-svg';


import store from '../../redux';
import State from '../../redux/actions/StateActions';

import Modal from 'react-native-modal';

const AuthScreen = (props) => {
    const navigation = useNavigation();
    const [phone, setPhone] = useState({text: ''});
    const [code, setCode] = useState({text: ''});
    const [phoneStatus, setPhoneStatus] = useState(false);
    const [loadInfo, setLoad] = useState(false);

    const [timer, setTimer] = useState(null);
    const [timerSec, setTimerSec] = useState(60);

    const [torchMode, setTorchMode] = useState('off');
    const [cameraType, setCameraType] = useState('back');
    const [ModalVisible, setModalVisibleMessage] = useState(false);

    // State({tabBar: false});
    // useEffect(() => {
    //     State({tabBar: false});
    // });
    function interval() {

    }

    useEffect(() => {

        State({tabBar: false});

    }, []);

    function barcodeReceived(e) {
        console.log('Barcode: ' + e.data);
        console.log('Type: ' + e.type);
    }


    return (
        <>
            <Modal
                onSwipeComplete={() => setModalVisibleMessage(false)}
                onBackdropPress={() => setModalVisibleMessage(false)}
                isVisible={ModalVisible}
                style={{margin: 0, justifyContent: 'center', alignItems: 'center', width: '100%'}}
                swipeDirection="down"
                propagateSwipe
                animationIn="slideInUp"
                animationOut="slideOutDown"
            >

                <View style={{flexDirection: 'column', flex: 1, height: '100%'}}>
                    <View style={{
                        flex: 1,
                        height: 200,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <Text style={{color: '#fff', fontSize: 18, textAlign: 'center'}}>Приложение не прошло
                            проверку
                            целостности и не может корректно работать, ожидайте</Text>

                        <TouchableOpacity
                            onPress={() => {

                            }}
                            style={{
                                width: '80%',
                                paddingTop: 10,
                                paddingBottom: 10,
                                backgroundColor: 'red',
                                marginTop: 60,
                            }}>
                            <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Обновить</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            <SafeAreaView style={[styles.container, styles.containerPaddingTop]}>

                <View style={styles.center}>
                    <View style={styles.fullBox}>

                        <Image
                            width={150}
                            height={150}
                            style={styles.logoAutc}
                            resizeMode="cover"
                            source={require('../../../assets/images/logo.png')}
                        />
                        <Text
                            style={[styles.autTitle, styles.text, styles.textBold, styles.text20, {marginLeft: -4}]}> Авторизация </Text>
                    </View>

                    {/*<BarcodeScanner*/}
                    {/*    onBarCodeRead={barcodeReceived}*/}
                    {/*    style={{flex: 1}}*/}
                    {/*    torchMode={torchMode}*/}
                    {/*    cameraType={cameraType}*/}
                    {/*/>*/}


                    <Input name={'ИИН'} place={'ХХХХ-ХХХХ-ХХХХ'} onChangeText={() => {

                    }}/>
                    <Input name={'Пароль'} place={'Пароль'} secret={true}/>

                    <View style={styles.fullBox}>
                        <View style={styles.textAuthContainer}>
                            <Text style={[styles.text, styles.textColor1, styles.text15]}>Забыли пароль?</Text>
                        </View>
                    </View>
                    <Btn click={() => {
                        //
                        setLoad(true);
                        setTimeout(() => {
                            setLoad(false);
                            State({tabBar: true});
                            navigation.navigate('Home');
                        }, 2200);
                    }} name={!loadInfo ? 'Войти' : (<ActivityIndicator size="small" color="#fff"/>)}/>
                    <View style={[styles.fullBox, styles.mt20]}>
                        <View>
                            <Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        // setTimeout(() => {
                                        //     setLoad(false);
                                        //     setModalVisibleMessage(true);
                                        // }, 1200);
                                        navigation.navigate('Register');
                                    }}>
                                    <Text>
                                        <Text style={[styles.text, styles.textColor3, styles.text15]}>У вас нету
                                            аккаунта? </Text>
                                        <Text
                                            style={[styles.text, styles.textColor1, styles.text15]}>Зарегистрироваться</Text>
                                    </Text>
                                </TouchableOpacity>
                            </Text>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        </>
    )
        ;
};


const styles = StyleSheet.create(
    {
        ...{
            container: {
                flex: 1,
            },
            center: {
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-start',
            },
            scrollView: {
                // paddingHorizontal: 15
            },
        },
        ...Css(),
    },
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps)(AuthScreen);
