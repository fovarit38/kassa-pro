import React, {useEffect} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator, Alert,
} from 'react-native';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// Redux Actions
import {removeError} from '../redux/actions/AppActions';

// Components
import Header from '../components/general/Header';
import AddEmployees from '../components/form/AddEmployees';
import Rem from '../utils/rem';

//css
import Css from '../utils/css/index';
import {API_PATH} from '../../config';
import axios from 'axios';
import Modal from 'react-native-modal';
import InputSearch from '../components/input/inputSearch';
import Btn from '../components/btn/btn';
import {style} from 'redux-logger/src/diff';
import {request} from 'react-native-permissions';

class StorageScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            model: false,
            catalogs: [],
            orders: [],
        };


    }

    componentDidMount() {


    }

    setModalVisibleMessage(event) {
        this.setState({model: event});
    }

    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <ScrollView>

                        <Header icon={4} next={false} name={'Оплатить налоги'}/>

                        <View style={[styles.container_def, styles.containerBottoms, {marginTop: Rem(0)}]}>

                            {
                                [
                                    ['Обязательные пенсионные взносы'],
                                    ['Индивидуальный подоходный налог'],
                                    ['Социальные отчисления'],
                                    ['Отчисления на обязательное соц. мед. страхование'],
                                    ['Взносы на обязательное соц. мед. страхование'],
                                ].map((item) => {
                                    return (
                                        <TouchableOpacity
                                            onPress={() => {
                                                // Alert.alert("ошибка интеграции вы не можете сдать "+item)
                                                // {item[1]}
                                                this.props.navigation.navigate('EmployeesSingleNalogPay');

                                            }}
                                            style={[styles.btnCons, styles.btnConsBig]}>
                                            <Text
                                                style={[styles.text, styles.text15, styles.textColor1A1C1E, {
                                                    marginLeft: Rem(10),
                                                    paddingRight: Rem(60),
                                                }]}>
                                                {item[0]}
                                            </Text>
                                            <Image
                                                style={[styles.backImg, {marginLeft: 'auto'}]}
                                                resizeMode="contain"
                                                source={require('../../assets/images/u_angle-right.png')}
                                            />
                                        </TouchableOpacity>
                                    );
                                })
                            }
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
};


const styles = StyleSheet.create(
    {
        ...
            {
                container: {
                    flex: 1,
                }
                ,
                scrollView: {
                    paddingHorizontal: 16,
                }
                ,
            }
        ,
        ...
            Css(),

    }
    ,
);

const mapStateToProps = state => ({
    selected_product: state.state.selected_product,
    employees: state.state.employees,
    state_info: state.state,
    is_auth: state.state.is_auth,
});

export default connect(mapStateToProps, null)(StorageScreen);
