var React = require('react-native')
    , Dimensions = React.Dimensions
    , {width, height} = Dimensions.get('window');


function Vw(size) {
    return '' + ((size / 375) * 100) + '%';
}

export default Vw;
