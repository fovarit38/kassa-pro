import Vw from '../../../src/utils/vw';
import Rem from '../../../src/utils/rem';
import {Dimensions} from 'react-native';

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

function Css() {

    return {
        container: {
            backgroundColor: '#fff',
            flex: 1,
        },
        containerPaddingTop: {
            paddingTop: Rem(20),
        },
        fullBox: {
            width: '100%',
            paddingLeft: Rem(16),
            paddingRight: Rem(16),

        },
        autTitle: {
            paddingTop: Rem(20),
            paddingBottom: Rem(20),
        },
        banner: {
            width: Rem(214),
            height: Rem(120),
            backgroundColor: '#DFF3FE',
            borderRadius: Rem(10),
            marginLeft: Rem(16),
            marginBottom: Rem(20),
            position: 'relative',
        },
        btnNavigation: {
            width: Rem(164),
            height: Rem(130),
            backgroundColor: '#DFF3FE',
            borderRadius: Rem(10),
            padding: Rem(15),
            overflow: 'hidden',
        },
        btnNavigationW3: {
            width: Rem(343),
            marginTop: Rem(30),
        },
        catalogBox: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            flexWrap: 'wrap',
            alignItems: 'center',
            marginTop: Rem(30),
        },
        logoAutc: {
            width: Rem(176),
            height: Rem(43),
        },
        safe: {
            flex: 1,
            backgroundColor: '#F1F3F6',
            position: 'relative',
        },
        scroll: {
            flex: 1,
            position: 'relative',
        },
        basket: {
            marginTop: Rem(20),
            marginBottom: Rem(20),
        },
        scrollFull: {
            minHeight: height,
            flex: 1,
            width: '100%',
        },
        flexGrow: {
            flex: 1,
            width: '100%',
        },
        container_def: {
            width: '100%',
            paddingLeft: Rem(15),
            paddingRight: Rem(15),
        },
        headers: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingTop: Rem(25),
            paddingBottom: Rem(25),
        },
        product_img: {
            width: '100%',
            height: Rem(253),
        },
        icon: {
            resizeMode: 'contain',
        },
        logoWrapper: {
            position: 'absolute',
            flex: 1,
            left: Rem(123),
            alignItems: 'center',
        },
        fullIm: {
            position: 'absolute',
            width: '100%',
            height: '100%',
            left: 0,
            top: 0,
        },
        logo: {
            width: 150,
            height: 32,
            resizeMode: 'contain',
        },
        text: {},
        text34: {
            fontSize: Rem(34),
        },
        text13: {
            fontSize: Rem(13),
        },

        textBold: {
            fontWeight: 'bold',
        },
        textColor1: {
            color: '#009FF5',
        },
        textColor919191: {
            color: '#919191',
        },
        textColor1A1C1E: {
            color: '#1A1C1E',
        },
        textAuthContainer: {
            width: Rem(342),
            marginBottom: Rem(20),
            alignItems: 'flex-end',
        },
        mt20: {
            marginTop: Rem(20),
        },
        modelAlert: {
            width: Rem(270),
            height: Rem(171),
            backgroundColor: '#fff',
            borderRadius: Rem(10),
            overflow: 'hidden',
        },
        bodyModel: {
            flex: 1,
            padding: Rem(1),
            textAlign: 'center',
            justifyContent: 'center',
            alignItems: 'center',
        },
        btnAlert: {
            width: '100%',
            height: Rem(44),
            backgroundColor: '#009FF5',
            justifyContent: 'center',
            alignItems: 'center',
        },
        faqControl: {
            width: Rem(343),
            height: Rem(50),
            shadowColor: '#000',
            backgroundColor: '#fff',
            borderWidth: 1,
            borderColor: '#e4e4e4',
            shadowOffset: {
                width: 0,
                height: 0,
            },
            shadowOpacity: 0.05,
            shadowRadius: 3.84,
            elevation: 1,
            borderRadius: Rem(10),
            justifyContent: 'center',
            paddingLeft: Rem(15),
        },
        mb20: {
            marginBottom: Rem(20),
        },
        homeTitle: {
            marginBottom: Rem(14),
            marginTop: Rem(14),
        },
        hr_line: {
            width: '100%',
            height: Rem(16),
            marginTop: Rem(14),
            marginBottom: Rem(14),
            position: 'relative',
            justifyContent: 'center',
            alignItems: 'center',
        },
        textColor2: {
            color: '#fff',
        },
        textColor3: {
            color: '#1A1C1E',
        },
        textColor4: {
            color: '#898A8D',
        },
        text18: {
            fontSize: Rem(18),
        },
        text10: {
            fontSize: Rem(10),
        },
        text17: {
            fontSize: Rem(17),
        },
        text16: {
            fontSize: Rem(16),
        },
        text15: {
            fontSize: Rem(15),
        },
        text14: {
            fontSize: Rem(14),
        },
        text12: {
            fontSize: Rem(12),
        },
        text20: {
            fontSize: Rem(20),
        },
        textCenter: {
            textAlign: 'center',
        },
        mt50: {
            marginTop: Rem(50),
        },
        product_margin_b20: {
            marginBottom: Rem(20),
        },
        product_margin_b15: {
            marginBottom: Rem(15),
        },
        btn_def: {
            backgroundColor: '#C82F5C',
            borderRadius: Rem(62),
            paddingTop: Rem(11),
            paddingBottom: Rem(11),
            alignItems: 'center',
        },
        btn_def_mini: {
            padding: Rem(13),
            paddingTop: Rem(5),
            paddingBottom: Rem(5),
        },
        basket_btns: {
            alignItems: 'flex-start',
            marginTop: Rem(15),
        },
        basket_product: {
            flexDirection: 'row',
            shadowColor: '#000',
            backgroundColor: '#fff',
            shadowOffset: {
                width: 0,
                height: 0,
            },
            shadowOpacity: 0.05,
            shadowRadius: 3.84,
            elevation: 7,
            borderRadius: Rem(16),
            overflow: 'hidden',
            marginBottom: Rem(15),
        },
        dopInfo: {
            marginTop: Rem(15),
        },
        basket_product_img: {
            width: Rem(160),
            height: Rem(150),
            borderRadius: Rem(16),
            overflow: 'hidden',
            marginRight: Rem(16),
        },
        basket_products: {
            marginTop: Rem(35),
        },
        basket_nav: {
            width: Rem(125),
            height: Rem(38),
            backgroundColor: '#F6F6FC',
            borderRadius: Rem(16),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingLeft: Rem(6),
            paddingRight: Rem(6),
        },
        basket_info: {
            justifyContent: 'space-between',
            paddingTop: Rem(5),
            paddingBottom: Rem(15),
        },
        btnInst: {
            width: Rem(24),
            height: Rem(24),
            backgroundColor: '#FFFFFF',
            borderWidth: 1,
            borderColor: '#DE374B',
            borderRadius: Rem(62),
            alignItems: 'center',
            justifyContent: 'center',
        },
        btnInstIcon: {
            width: Rem(8),
            height: Rem(8),
            resizeMode: 'contain',
        },
        promo: {
            width: Rem(175),
            height: Rem(38),
            alignItems: 'center',
            textAlign: 'center',
            backgroundColor: '#F7F6FE',
            borderRadius: Rem(62),
        },
        icon_secret: {
            width: Rem(22),
            height: Rem(16),
            right: Rem(14),
            top: Rem(14),
            position: 'absolute',
        },
        textBottom: {
            position: 'absolute',
            width: '100%',
            bottom: 0,
            left: 0,
            padding: Rem(10),
        },
        activeRadio: {
            width: Rem(24),
            height: Rem(24),
            right: Rem(14),
            top: Rem(10),
            position: 'absolute',
        },
        backImg: {
            width: Rem(25),
            height: Rem(25),
        },
        backImgCntr: {
            width: '100%',
            height: '100%',
            right: 0,
            bottom: 0,
            position: 'absolute',
        },
        phoneInput: {
            width: Rem(343),
            height: Rem(44),
            alignItems: 'center',
            textAlign: 'left',
            backgroundColor: '#F7F7F7',
            borderRadius: Rem(10),
            // borderColor: '#DEDEDE',
            // borderWidth: 1,
            paddingLeft: Rem(18),
            color: '#979797',
        },
        codeInput: {
            width: Rem(185),
            height: Rem(63),
            alignItems: 'center',
            textAlign: 'center',
            backgroundColor: '#F7F6FE',
            borderRadius: Rem(16),
            borderColor: '#000000',
            borderWidth: 1,
            color: '#979797',
        },
        inputTel: {
            position: 'relative',
            marginBottom: Rem(18),
        },
        authTopImg: {
            height: Rem(210),
            resizeMode: 'contain',
            marginBottom: Rem(-85),
        },
        authBottomImg: {
            height: Rem(240),
            resizeMode: 'contain',
            marginTop: Rem(-85),
        },
        hr_line_border: {
            backgroundColor: '#ACACAE',
            position: 'absolute',
            left: 0,
            top: Rem(10),
            height: 1,
            width: '100%',
        },
        btnAuth: {
            width: Rem(342),
            height: Rem(44),
            backgroundColor: '#009FF5',
            borderRadius: Rem(5),
            alignItems: 'center',
            justifyContent: 'center',
        },
        btn_catalog: {
            backgroundColor: '#009FF5',
            width: Rem(343),
            paddingLeft: Rem(7),
            paddingRight: Rem(0),
            paddingBottom: Rem(8),
            paddingTop: Rem(8),
            borderRadius: Rem(4),
            marginBottom: Rem(5),
            flexDirection: 'row',
            alignItems: 'center',
        },
        orderBox: {
            width: '100%',
            padding: Rem(16),
            borderTopWidth: 1,
            borderTopColor: '#efefef',
        },
        orderHead: {
            flexDirection: 'row',
        },
        orderHeadImg: {},
        orderHeadInfo: {
            marginLeft: Rem(8),
        },
        infoList: {
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: Rem(4),
        },
        orderbody: {
            width: '100%',
            marginTop: Rem(10),
        },
        orderImg: {
            width: Rem(128),
            height: Rem(96),
            borderRadius: Rem(5),
        },
        infoListIcon: {
            width: Rem(11),
            height: Rem(11),
            resizeMode: 'contain',
            marginLeft: Rem(11),
        },
        catalog_icon: {
            width: Rem(16),
            height: Rem(16),
            marginRight: Rem(12),
            resizeMode: 'contain',
        },
        catalog_icon_right: {
            width: Rem(8),
            height: Rem(16),
            marginRight: Rem(12),
            resizeMode: 'contain',
        },
        btn_catalog_list: {},
        catalogCount: {
            marginLeft: 'auto',
            marginRight: Rem(17),
        },
        logoText: {
            flex: 1,
            textAlign: 'center',
        },
        logoTextHead: {},
        overfloa: {
            position: 'absolute',
            left: 0,
            top: 0,
            height: '100%',
            width: '100%',
            opacity: 0.5,
        },
        inputTelCode: {
            position: 'absolute',
            left: 0,
            top: 0,
            height: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputTelCodeRight: {
            position: 'absolute',
            right: 0,
            top: 0,
            height: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputTelCodeBox: {
            height: Rem(30),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            borderRightColor: '#000000',
            borderRightWidth: 1,
            paddingLeft: Rem(30),
            paddingRight: Rem(25),
        },
        inputTelCodeBoxRight: {
            height: Rem(37),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            paddingRight: Rem(15),
        },
        itog: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        btnsMore: {
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        btn_def_link: {
            marginTop: Rem(15),
            alignItems: 'center',
        },
        btn_chcc: {
            alignItems: 'flex-start',
            backgroundColor: '#fff',
            shadowOffset: {
                width: 0,
                height: 0,
            },
            shadowOpacity: 0.05,
            shadowRadius: 3.84,
            elevation: 2,
            padding: Rem(13),
            marginBottom: Rem(15),
        },
        btn_chcc_active: {
            elevation: 0,
            backgroundColor: '#C82F5C',
        },
        btn_auth: {},
        btn_auth_icon: {
            width: Rem(37),
            height: Rem(37),
        },
        mtauto: {
            marginTop: Rem(60),
        },
        moreHead: {
            backgroundColor: '#C82F5C',
            paddingTop: Rem(16),
            paddingBottom: Rem(16),
        },
        userIcon: {
            width: Rem(24),
            height: Rem(24),
            marginRight: Rem(8),
        },
        btnIcon1: {
            width: Rem(26),
            height: Rem(26),
            marginBottom: Rem(5),
            resizeMode: 'contain',
        },
        moreHeadIcon: {
            flexDirection: 'row',
            alignItems: 'center',
        },
        moreHeadBody: {
            marginTop: Rem(36),
            marginBottom: Rem(26),
        },
        btn_1: {
            width: Rem(80),
            height: Rem(73),
            backgroundColor: 'rgba(255, 255, 255, 0.31)',
            borderRadius: Rem(16),
            justifyContent: 'center',
            alignItems: 'center',
        },
        link: {
            width: '100%',
            marginTop: Rem(24),
            marginBottom: Rem(12),
        },
        iconProfile: {
            width: Rem(9),
            height: Rem(15),
        },
        profiles: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: Rem(20),
            marginBottom: Rem(20),
        },
        profilesSrocks: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: Rem(20),
            marginBottom: Rem(20),
            position: 'absolute',
            left: 0,
            top: 0,
        },
        textProfil: {
            marginRight: 'auto',
            marginLeft: Rem(40),
        },
        iconProfileExit: {
            width: Rem(16),
            height: Rem(16),
        },
        profiles_main: {
            marginTop: Rem(15),
        },
        addAdress: {
            height: Rem(120),
            width: Rem(120),
            shadowColor: '#000',
            paddingTop: Rem(15),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#fff',
            shadowOffset: {
                width: 0,
                height: 0,
            },
            shadowOpacity: 0.05,
            shadowRadius: 3.84,
            elevation: 7,
            borderRadius: Rem(16),
            overflow: 'hidden',
        },
        profiles_adres_list: {
            marginTop: Rem(20),
            marginBottom: Rem(20),
        },
        bigPLus: {
            height: Rem(43),
            width: Rem(43),
            marginBottom: Rem(15),
        },
        input_def: {
            borderBottomWidth: 1,
            borderBottomColor: '#D8D8D8',
        },
        codeInput_def: {
            width: '100%',
            color: '#979797',
        },
        containerBottoms: {
            marginBottom: Rem(90),
        },
        profiles_main_top: {
            marginTop: Rem(46),
        },
        search_input: {
            width: Rem(250),
            marginRight: 'auto',
            marginLeft: 0,
        },
        evployeesBox: {},
        evployeeSingle: {
            width: Rem(343),
            // height: Rem(106),
            marginTop: Rem(20),
            borderWidth: 1,
            borderColor: '#ccc',
            borderRadius: Rem(10),
            padding: Rem(15),
        },
        evployeeSingle_name: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        },
        evployeeSingle_info: {
            flexDirection: 'row',
            marginTop: Rem(15),
        },
        infoEmployess: {
            width: '50%',
        },
        infoEmployessmini: {

        },
        infoEmployess_val: {
            marginTop: Rem(5),
        },
        btnCons: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: Rem(343),
            height: Rem(44),
            borderWidth: 1,
            borderColor: 'rgba(0, 0, 0, 0.09)',
            borderRadius: Rem(10),
            padding: Rem(10),
            marginTop: Rem(20),
        },
        btnConsBig: {
            borderWidth: 0,
            borderBottomWidth: 1,
            paddingHorizontal: 0,
            paddingVertical: Rem(17),
            height: 'auto',
            marginTop: Rem(0),
        },
    };
}

export default Css;
