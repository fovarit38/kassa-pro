import React from 'react';
import {
    Text,
    TextInput,
    StyleSheet,
    ScrollView,
    View, Image, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import store from '../../src/store/store';
import Css from '../../src/css/index';
import Modal from 'react-native-modal';


class Input extends React.Component {

    constructor() {
        super();

        this.state = {
            phone_modal: false,
            country: [],
            country_set: [],
        };
    }

    setPhoneModalVisible(event) {
        this.setState({phone_modal: event});
    }

    render() {
        var type = typeof this.props.type != 'undefined' ? this.props.type : 'text';
        var keyboard = null;

        var icon = null;
        return (
            <View style={styles.input}>
                <TextInput  placeholderTextColor="#000"  style={styles.input_in} keyboardType={keyboard}
                           onChangeText={typeof this.props.onChangeText != 'undefined' ? this.props.onChangeText : null}
                           placeholder={typeof this.props.placeholder != 'undefined' ? this.props.placeholder : ''}>{typeof this.props.value != 'undefined' ? this.props.value : ''}</TextInput>
            </View>
        );

    }
}


const styles = StyleSheet.create(Css());

export default Input;
