var React = require('react-native')
    , Dimensions = React.Dimensions
    , {width, height} = Dimensions.get('window');

var units = {
    vw: width / 100
    , vh: height / 100
    , vhSelf: (width / 375),
};

function Rem(size) {
    return (size * units.vhSelf);
}

export default Rem;
