import axios from 'axios';
import {API_PATH} from '../../config';

import State from '../redux/actions/StateActions';


class Api {


    static async request(request) {

        request = Object.assign({
            method: 'GET',
            url: '',
        }, request);

        request.url = API_PATH + request.url;

        return await axios(request).then((response) => {
            return response.data.data;
        }).catch(function (error) {
            return [];
        });
    }

    static async requestCustomUrl(request) {

        request = Object.assign({
            method: 'GET',
            url: '',
        }, request);


        return await axios(request).then((response) => {

            return response.data;
        }).catch(function (error) {
            return [];
        });
    }

    static async getAll() {
        return await axios.get(`${API_PATH}/categories`);
    }


    static async getProducts(categoryId) {
        State({menu_send: true});
        let get = axios.get(`${API_PATH}/categories/${categoryId}/products`);
        get.then(({data}) => {
            if (typeof data === 'string') {
            } else if (data.error) {
            } else {
                if (data.items.length > 0) {
                    State({selected_catalog_products: data.items});
                }
            }
            State({menu_send: false});
        }).catch(error => {
            State({menu_send: false});
        });
    }

    static async getBearCode(code) {


        return await this.requestCustomUrl({
            method: 'GET',
            url: `https://cicada.kz/?send=` + code,
        });
    }

    static async getAllCategories() {
        return await this.request({
            method: 'GET',
            url: `/database/get/Catalog`,
        });
    }

    static async getUserAuth(token) {
        return await this.request({
            method: 'GET',
            headers: {
                Accept: 'application/json',
                Authorization: 'Bearer ' + token,
            },
            url: `/auth/user`,
        });

    }


    static async authSendCode(phone, code) {
        return await this.request({
            method: 'GET',
            url: `/auth/mobile/login?login=` + phone + (typeof code != 'undefined' ? '&code=' + code : ''),
        });
    }
}

export default Api;
